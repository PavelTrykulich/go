<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'title' =>'Php is awesome',
                'short_description' => 'Article about Php language',
                'body' => '1 2 3',
            ],
            [
                'title' =>'2Php is awesome',
                'short_description' => 'Article about Php language',
                'body' => '1 2 3',
            ],
            [
                'title' =>'3Php is awesome',
                'short_description' => 'Article about Php language',
                'body' => '1 2 3'
        ]
        ]);
    }
}
