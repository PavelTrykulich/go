<?php
/**
 * Created by PhpStorm.
 * User: Pasha
 * Date: 13.05.2018
 * Time: 17:39
 */

class Model
{   public $db;

    public function __construct()
    {
        try{
            $this->db = new PDO('mysql:host=localhost;dbname=portfolio',
                'root');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db->exec('SET NAMES "utf8"');
        }catch(Exception $e){
            echo "Не удалось подключиться к бд!";
            die();
        }
    }



}