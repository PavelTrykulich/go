<?php
/**
 * Created by PhpStorm.
 * User: Pasha
 * Date: 13.05.2018
 * Time: 16:22
 */

class View
{
    public $templateView;

    public function generate($contentView,$templateView,$data = NULL)
    {
        if(is_array($data)){
            extract($data);
        }
     include 'application/views/' . $templateView;
    }
}