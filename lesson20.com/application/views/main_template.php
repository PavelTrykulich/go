<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>

<nav class="navbar navbar-expand-lg  navbar navbar-dark bg-dark">

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a href="/" class="nav-link"><h5>Home page</h5></a>
            </li>
            <li class="nav-item active">
                <a href="/Main/contacts" class="nav-link"><h5>Contacts page</h5></a>
            </li>
            <li class="nav-item active">
                <a href="/Portfolio" class="nav-link"><h5>Portfolio</h5></a>
            </li>
        </ul>
    </div>
</nav>

<hr>
<div>
    <?php include_once $contentView;?>
</div>
<hr>

<div style="text-align: center;">
    <div id="footer" class="alert alert-dark" role="alert">
        <span>Trykulich Pasha HomeWork 17 (c) 2018</span>
    </div>
</div>
</body>
</html>