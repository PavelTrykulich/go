<?php


class Controller_Main extends Controller
{
    public function action_index(){
        $this->view->generate('homepage.php','main_template.php');
    }

    public function action_contacts(){
        $this->view->generate('contacts.php', 'main_template.php');
    }
}