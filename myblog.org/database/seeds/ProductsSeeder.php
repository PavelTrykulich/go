<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
         [
            'title' => 'Php for beginers',
            'slug' => 'Php_for_beginers',
            'price' => 20.2,
            'description' => 'text',
         ],
         [
            'title' => 'Php 7.2 for beginers',
            'slug' => 'Php_7_2',
            'price' => 20.2,
            'description' => '72text',
         ],
         [
            'title' => 'Js for beginers',
            'slug' => 'Js_for_beginers',
            'price' => 26.2,
            'description' => 'text',
         ]
        ]);
    }
}
