@extends('layouts/main')

@section('main_content')
    @foreach($products as $product)
        <div class="col-md-4">
            <h2>{{ $product->title }}</h2>
            <p>{{ $product->price }}</p>
            <p><a class="btn btn-danger" href="/posts/{{$product->slug}}/delete" role="button">Delete &raquo;</a></p>
        </div>
    @endforeach

@endsection

@section('jumbotron')
    <h1 class="display-3">Posts</h1>
    <p>Hillel blog:</p>
@endsection