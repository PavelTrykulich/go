@extends('layouts.main')

@section('jumbotron')
    <h1 class="display-3">{{$post->title}}</h1>
    <p>{{$post->short_description}}</p>
@endsection

@section('main_content')
    <div class="col-md-12">
        <p>{{$post->body}}</p>
    </div>

    <div class="comments col-md-12">
        <ul class="list-group">
            @foreach($post->comments as $comment)
                <li class="list-group-item">
                    {{$comment->created_at->diffForHumans()}}
                    <strong>{{$comment->body}}</strong></li>
            @endforeach
        </ul>
    </div>


    <div class="col-md-12 bg-warning">
        @include('layouts.embed.errors')
        <form action="/posts/{{$post->slug}}/comments" method="post">
            {{csrf_field()}}
            {{method_field('put')}}
            <div class="form-group">
                <label for="body">Enter your comment</label>
                <textarea name="body" id="body"  class="form-control">{{old('body')}}</textarea>
            </div>
            <button type="submit" class="btn btn-success">Post comment</button>
        </form>
    </div>
@endsection