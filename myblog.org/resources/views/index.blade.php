@extends('layouts/main')

@section('main_content')
    @foreach($posts as $post)
        <div class="col-md-4">
            <h2>{{ $post->title }}</h2>
            <p>{{ $post->short_description }}</p>
            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
        </div>
    @endforeach

@endsection

@section('jumbotron')
    <h1 class="display-3">Main page!</h1>
    <p>This is hillel blog website.</p>
@endsection