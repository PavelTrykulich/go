<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'slug', 'short_description', 'body'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function addComment($body){
        Comment::create([
            'body' => $body,
            'post_id' => $this->id
        ]);
    }
}
