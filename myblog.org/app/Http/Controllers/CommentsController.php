<?php

namespace App\Http\Controllers;

use App\Post;
use App\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function store(Post $post){
        $this->validate(request(),[
            'body' => 'required|min:4|max:255'
        ]);

        /*Comment::create([
            'body' => request()->post('body'),
            'post_id' => $post->id
        ]);*/

        $post->addComment(request()->post('body'));
        return back();
    }
}
