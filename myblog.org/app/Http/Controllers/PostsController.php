<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function index(){
        $posts = Post::all();
        return view('posts.index')->with(compact('posts'));
    }

    public function show(Post $post){
        return view('posts.show')->with(compact('post'));
    }

    public function edit(Post $post){
        return view('posts.edit')->with(compact('post'));
    }

    public function update(Post $post){
        $this->validate(request(),[
            'title' => 'required|max:25',
            'slug' => 'required|unique:posts,slug,'.$post->id.',id',
            'short_description' => 'required',
            'body' => 'required'
        ]);
        $post->update(request()->post());
        // Redirect user to /posts
        return redirect('/posts');
    }

    public function delete(Post $post){
        return view('posts.delete')->with(compact('post'));
    }

    public function destroy(Post $post){
        $post->delete();
        return redirect('/posts');
    }

    public function create(){
        return view('posts.create');
    }

    public function store(){
        // Create new Post

        $this->validate(request(),[
            'title' => 'required|max:25',
            'slug' => 'required|unique:posts,slug',
            'short_description' => 'required',
            'body' => 'required'
        ]);

        Post::create(request()->post());
        // Redirect user to /posts
        return redirect('/posts');
    }
}
