<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
    return ['page'=>'Home page'];
});

Route::get('/posts', function (){
    $message = 'Hello world!';
    dd(['page'=>'Home page', 'elements' => [1,2,3]]);
    echo '123';
    return $message;
});

Route::get('/contacts', function(){
    $userName = 'George';
    $data = [];
    $data['userName'] = $userName;
    $data['userType'] = 'admin';
    return view('test')->with($data);
});
