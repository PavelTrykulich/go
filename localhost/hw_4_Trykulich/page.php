<?php
$tovar = [['title' => 'Бургер',
           'price' => 60,
           'category' => 'food' 
          ],
          ['title' => 'Шаурма',
           'price' => 50,
           'category' => 'food' 
          ],
          ['title' => 'Суши',
           'price' => 70,
           'category' => 'food' 
          ],
          ['title' => 'Пицца',
           'price' => 150,
           'category' => 'food' 
          ],
          ['title' => 'Факачча',
           'price' => 20,
           'category' => 'food' 
          ],
          ['title' => 'Кола',
           'price' => 20,
           'category' => 'drink' 
          ],
          ['title' => 'Кофе',
           'price' => 25,
           'category' => 'drink' 
          ],
          ['title' => 'Чай',
           'price' => 15,
           'category' => 'drink' 
          ],
          ['title' => 'Глинтвейн',
           'price' => 40,
           'category' => 'drink' 
          ],
          ['title' => 'Сок',
           'price' => 30,
           'category' => 'drink' 
          ]
          
         ];
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Задание №2</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
 <!--Heder block start-->
  <nav class="navbar navbar-expand-lg  navbar navbar-dark bg-primary">
      <a href="https://odessa.ithillel.ua/" class="navbar-brand">
         <img src="images/navbar.jpg" width="75" height="35" alt="logo">
      </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" 
        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" 
        aria-expanded="false" aria-label="Toggle navigations">
        <span class="navbar-toggler-icon"></span>
      </button> 
       <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a href="index.php" class="nav-link"><h5>Задание №1</h5></a>  
          </li>
          <li class="nav-item active">
            <a href="page.php" class="nav-link"><h5>Задание №2</h5></a> 
          </li>
          <li class="nav-item active">
            <a href="page2.php" class="nav-link"><h5>Дополнительное задание</h5></a>  
          </li>       
        </ul>
      <form class="form-inline my-2 my-lg-0">
        <input type="text" class="form-control mr-sm-2" placeholder="Поиск" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0">Поиск</button>
      </form>       
         </div>
</nav>
<!--Heder block end-->

<!-- Content block start -->
<div>
<p ><h2 class="text"><center>Таблица товаров</center></h2></p>
<table class="table">
      <thead class="table-primary">
       <tr>
          <th scope="col"></th>
          <th scope="col"><h5>Название товара</h5></th>
          <th scope="col"><h5>Категория</h5></th>
          <th scope="col"><h5>Цена</h5></th>
       </tr>
        </thead>
        <tbody class="font-weight-bold">     
                      
                     <?php 
                        foreach ($tovar as $key => $value){?> 
                         <tr <?php
                         if($key % 2 == 0){//use goto and if for change the color of the rows
                              goto a;
                           } ?>
                         class="table-secondary"
                         <?php a: ?>>                            
                             <td></td>                                                        
                             <td><?=$value['title']?></td> 
                             <td><?=$value['category']?></td>
                             <td><?=$value['price'] . " грн"?></td>
                           </tr> 
                          <?}?> 
                                                                                            
         </tbody>
    </table>
</div>  
<!--Content block end-->

<!--Footer block start-->
    <center>
    <div id="footer" class="alert alert-primary" role="alert">
      <span>Trykulich Pasha HomeWork 4 (c) 2018</span>
    </div>
  </center>
  <!--Footer block end-->
</body>
</html>