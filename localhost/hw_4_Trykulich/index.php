<?php
  $price_commercial_EUR = 33.2481;
  $price_commercial_USD = 27.036;
  
  $UAH = 1000;
  $EUR;
  $USD;

  $control=2;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Задание №1</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
 <!--Heder block start-->
	<nav class="navbar navbar-expand-lg  navbar navbar-dark bg-primary">
  		<a href="https://odessa.ithillel.ua/" class="navbar-brand">
    		 <img src="images/navbar.jpg" width="75" height="35" alt="logo">
  		</a>
  	<button class="navbar-toggler" type="button" data-toggle="collapse" 
  			data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" 
  			aria-expanded="false" aria-label="Toggle navigations">
    		<span class="navbar-toggler-icon"></span>
    	</button> 
    	 <div class="collapse navbar-collapse" id="navbarSupportedContent">
    		<ul class="navbar-nav mr-auto">
    			<li class="nav-item active">
    				<a href="index.php" class="nav-link"><h5>Задание №1</h5></a>	
    			</li>
    			<li class="nav-item active">
    				<a href="page.php" class="nav-link"><h5>Задание №2</h5></a>	
    			</li>
    			<li class="nav-item active">
    				<a href="page2.php" class="nav-link"><h5>Дополнительное задание</h5></a>	
    			</li>				
    		</ul>
			<form class="form-inline my-2 my-lg-0">
				<input type="text" class="form-control mr-sm-2" placeholder="Поиск" aria-label="Search">
				<button class="btn btn-outline-success my-2 my-sm-0">Поиск</button>
			</form>    		
         </div>
</nav>
<!--Heder block end-->

<!-- Content block start -->

<div>
	<p ><h2 class="text"><center>Таблица конвертации гривны в доллары и евро </center></h2></p>	
	  
		<table class="table">
  			<thead class="table-primary">
   		 <tr>
   		 	<th scope="col"></th>
      		<th scope="col"><h5>UAH</h5></th>
      		<th scope="col"><h5>EUR</h5></th>
      		<th scope="col"><h5>USD</h5></th>
   		 </tr>
  			</thead>
 			  <tbody class="font-weight-bold">     
                        <?php 
                   		while($UAH <= 2000) { //loop for output
                   	    	$EUR = $UAH/$price_commercial_EUR;
                   	    	$USD = $UAH/$price_commercial_USD;?>
                     	<tr 
                     	   <?php
                     	   if($control % 2 == 0){//use goto and if for change the color of the rows
                     	   	    goto a;
                     	     } ?>
                     	   class="table-secondary"
                     	   <?php a: ?>
                     	>  
                   		     <td></td>
                   	         <td><?=round($UAH,2)?></td>     
     			  			 <td><?=round($EUR,2)?></td>    
      		 	 			 <td><?=round($USD,2)?></td>
      		 	        </tr>
      		 	 	    	 <?php $UAH +=50;
      		 	 	    	       $control++; 
      		 	             }?>                            		 
  			 </tbody>
		</table>
</div>	
<!--Content block end-->

<!--Footer block start-->
    <center>
		<div id="footer" class="alert alert-primary" role="alert">
			<span>Trykulich Pasha HomeWork 4 (c) 2018</span>
		</div>
	</center>
	<!--Footer block end-->
</body>
</html>