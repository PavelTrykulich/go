<?php
class AudioProduct extends ShopProduct{
	public $playTime = 0;

	public function __construct($title, $firstName, $lastName, $price, $playTime){
		parent::__construct($title, $firstName, $lastName, $price);
		$this->playTime = $playTime;
	}

	public function getSummaryLine(){
		$result = '';
		$result .= parent::getSummaryLine();
		$result .= ' ' . $this->playTime;
		return $result;
	}

	public function makeDiscount($discount){
		return $this->price - $this->price * $discount /100;
	}
}