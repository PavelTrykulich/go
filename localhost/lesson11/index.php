<?php
require_once 'ShopProduct.Class.php';
require_once 'BookProduct.Class.php';
require_once 'AudioProduct.Class.php';
require_once 'ShopProductWriter.Class.php';

$bookProduct1 = new BookProduct('Php is awesome', 'John', 'Doe', 30, 450);
echo "<br>";
echo $bookProduct1->getSummaryLine();
echo "<br>";
echo "Price with discount: " . $bookProduct1->makeDiscount(20);

$audioProduct1 = new AudioProduct('Html in songs', 'Jack', 'Jones', 10, 360);
echo "<br>";
echo $audioProduct1->getSummaryLine();
echo "<br>";
echo "Price with discount: " . $audioProduct1->makeDiscount(20);

$writer = new ShopProductWriter();
echo $writer->write($bookProduct1);
echo $writer->write($audioProduct1);

