<?php
class ShopProduct{
	public $title = '';
	public $authorFirstName = '';
	public $authorLastName = '';
	public $price = 0;

	public function __construct($title, $firstName, $lastName, $price){
		$this->title = $title;
		$this->authorFirstName = $firstName;
		$this->authorLastName = $lastName;
		$this->price = $price;
	}

	public function fullName(){
		return $this->authorFirstName . ' ' . $this->authorLastName;
	}

	public function makeDiscount($discount){
		return $this->price - $discount;
	}

	public function getSummaryLine(){
		$result = '';
		$result .= $this->title;
		$result .= ' ' . $this->fullName();
		$result .= ' ' . $this->price;
		return $result;
	}

}