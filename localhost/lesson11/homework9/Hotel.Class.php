<?php
class Hotel{
	public $title = '';
	public $shortDescription = '';
	public $description = '';

	public function __construct($title, $shortDescription, $description){
		$this->title = $title;
		$this->shortDescription = $shortDescription;
		$this->description = $description;
	}

	public function getShortInfo(){
		return $this->title. ' ' . $this->shortDescription;
	}
}