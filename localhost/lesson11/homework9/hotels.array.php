<?php 
$hotelsRaw = [
	[
		'title' => 'Hilton',
		'shortDescription' => 'Something about this hotel',
		'description' => 'Complete information about hotel'
	],
	[
		'title' => 'Hotel 2',
		'shortDescription' => 'Something about this hotel',
		'description' => 'Complete information about hotel'
	],
	[
		'title' => 'Hotel 3',
		'shortDescription' => 'Something about this hotel',
		'description' => 'Complete information about hotel'
	],
	[
		'title' => 'Hotel 4',
		'shortDescription' => 'Something about this hotel',
		'description' => 'Complete information about hotel'
	],
	[
		'title' => 'Hotel 5',
		'shortDescription' => 'Something about this hotel',
		'description' => 'Complete information about hotel'
	],
];

$hotels = [];
foreach ($hotelsRaw as $hotel) {
	$hotels[] = new Hotel($hotel['title'], $hotel['shortDescription'], $hotel['description']);
}