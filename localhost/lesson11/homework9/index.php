<?php
require_once 'Hotel.Class.php';
require_once 'hotels.array.php';

?>
<!DOCTYPE html>
<html>
<head>
	<title>Hotels</title>
</head>
<body>
	<div>
		
		<ul>
			
			<?php foreach($hotels as $hotelId => $hotel):?>
				<li>
					<a href="hotel.php?id=<?=$hotelId?>"><?=$hotel->getShortInfo();?></a>
				</li>
			<?php endforeach;?>	

		</ul>

	</div>
	<?php include 'displayedHotels.php';?>
</body>
</html>
