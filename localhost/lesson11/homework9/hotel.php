<?php
require_once 'Hotel.Class.php';
require_once 'hotels.array.php';

if(!isset($_GET['id'])){
	header('Location:index.php');
}
$hotelId = $_GET['id'];
$hotel = $hotels[$hotelId];

$viewedHotels = [];
if(isset($_COOKIE['viewedHotels'])){
	$viewedHotels = json_decode($_COOKIE['viewedHotels'], true);
}
if(! in_array($hotelId, $viewedHotels)){
	$viewedHotels[]= $hotelId;
}
setcookie('viewedHotels', json_encode($viewedHotels), time()+60*60*2);



?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div>
		<ul>
			<li><?=$hotel->title?></li>
			<li><?=$hotel->shortDescription?></li>
			<li><?=$hotel->description?></li>
		</ul>
	</div>
	<?php include 'displayedHotels.php';?>
</body>
</html>