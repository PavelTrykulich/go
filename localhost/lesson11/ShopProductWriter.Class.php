<?php
class ShopProductWriter{

	public function write(ShopProduct $shopProduct){
		$str = '';
		$str = $shopProduct->title . ' ' . $shopProduct->fullName();
		$str .= ' price: '. $shopProduct->price;
		return $str;
	}
}