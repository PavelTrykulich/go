<?php
class BookProduct extends ShopProduct{
	public $pages = 0;

	public function __construct($title, $firstName, $lastName, $price, $pages){
		parent::__construct($title, $firstName, $lastName, $price);
		$this->pages = $pages;
	}

	public function getSummaryLine(){
		$result = '';
		$result .= parent::getSummaryLine();
		$result .= ' ' . $this->pages;
		return $result;
	}
}