<?php
require_once 'connection_db.php';

if(!isset($_POST['name_text'])) {
	header('Location:members_create.php');
	die();
}

$name_text = htmlspecialchars($_POST['name_text'], ENT_QUOTES, 'utf-8');
$grouphuman_text = htmlspecialchars($_POST['grouphuman_text'], ENT_QUOTES, 'utf-8');
$email_text = htmlspecialchars($_POST['email_text'], ENT_QUOTES, 'utf-8');
$phone_text = htmlspecialchars($_POST['phone_text'], ENT_QUOTES, 'utf-8');
$attendance_text = htmlspecialchars($_POST['attendance_text'], ENT_QUOTES, 'utf-8');
$averagescores_text = htmlspecialchars($_POST['averagescores_text'], ENT_QUOTES, 'utf-8');
$subjec_text = htmlspecialchars($_POST['subjec_text'], ENT_QUOTES, 'utf-8');
$workday_text = htmlspecialchars($_POST['workday_text'], ENT_QUOTES, 'utf-8'); 

try{
	$sql = 'INSERT INTO members SET
			name = "'.$name_text.'",
			grouphuman  = "'.$grouphuman_text.'",
			email = "'.$email_text.'",
			phone ="'.$phone_text.'",
			averagescores = "'.	$averagescores_text.'",
			attendance = "'.$attendance_text.'",
			subject = "'.$subjec_text.'",
			workday = "'.$workday_text.'"
	';
	$pdo->exec($sql);
	header('Location:index.php');

}catch(Exception $e){
	echo "Error adding test data" . $e->getMessage();
}