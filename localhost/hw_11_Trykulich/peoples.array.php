<?php
 
  $peoples = [['name' => 'Ethan O’Connor',
               'grouphuman' => 'student',
               'email' => 'Ethan@mail.com',
               'phone' => '123-456-78',
               'attendance'=> '4.5',
               'averagescores'=> '89%'
              ],
              ['name' => 'Dustin Horn',
               'grouphuman' => 'teacher',
               'email' => 'Dustin@mail.com',
               'phone' => '313-456-78',
               'subject' =>'economy'
              ],
              ['name' => 'Gary Bell',
               'grouphuman' => 'student',
               'email' => 'Gary@mail.com',
               'phone' => '985-436-98',
               'attendance'=> '5',
               'averagescores'=> '100%'
              ],
              ['name' => 'Wilfred King',
               'grouphuman' => 'admin',
               'email' => 'King@mail.com',
               'phone' => '653-356-23',
               'workday' => 'Tuesday'
              ],
              ['name' => 'Mark Carson',
               'grouphuman' => 'admin',
               'email' => 'Mark@mail.com',
               'phone' => '852-451-79',
               'workday' => 'Wednesday'
              ],
              ['name' => 'Leslie Greer',
               'grouphuman' => 'teacher',
               'email' => 'Leslie@mail.com',
               'phone' => '555-555-55',
               'subject' =>'psychology'
              ],
              ['name' => 'Robert Daniels',
               'grouphuman' => 'student',
               'email' => 'Robert@mail.com',
               'phone' => '122-543-21',
               'attendance'=> '4.4',
               'averagescores'=> '82%'
              ],
              ['name' => 'William Wilkerson',
               'grouphuman' => 'teacher',
               'email' => 'William@mail.com',
               'phone' => '908-765-56',
               'subject' =>'philosophy'
              ],
              ['name' => 'Tyrone McBride',
               'grouphuman' => 'student',
               'email' => 'Tyrone@mail.com',
               'phone' => '456-456-45',
               'attendance'=> '4',
               'averagescores'=> '69%'
              ],
              ['name' => 'Thomas Logan',
               'grouphuman' => 'admin',
               'email' => 'Thomas@mail.com',
               'phone' => '999-456-23',
               'workday' => 'Monday'
              ],
              ['name' => 'Baldric Young',
               'grouphuman' => 'teacher',
               'email' => 'Baldric@mail.com',
               'phone' => '319-316-54',
               'subject' =>'math'
              ]
                

  ];
?>