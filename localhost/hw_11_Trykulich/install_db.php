<?php
require_once 'connection_db.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Cоздать базу данных</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Header block start-->
<div>
    <? include "header.html";?>
</div>
<!--Header block end-->

<?php

try{
	$sql = 'CREATE TABLE members(
			id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			name TEXT,
			email TEXT,
			phone TEXT,
			grouphuman TEXT,
			attendance TEXT,
			averagescores TEXT,
			subject TEXT,
			workday TEXT
	)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
	$pdo->exec($sql);

}catch(PDOException $e){
	echo 'Не удалось создать таблицу members ' . $e->getMessage();
}?>

<!--Footer block start-->
<div>
    <? include "footer.html";?>
</div>
<!--Footer block end-->
</body>
</html>