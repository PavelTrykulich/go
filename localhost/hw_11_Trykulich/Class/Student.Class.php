<?php


class Student extends Person{
	public $attendance = '';
	public $averagescores = '';
	public function __construct($name,$grouphuman,$email,$phone,$attendance,$averagescores){
		parent::__construct($name,$grouphuman,$email,$phone);
		$this->attendance = $attendance;
		$this->averagescores = $averagescores;
	}
	public function getVisitCard(){
		$str = '';
		$str = parent::getVisitCard();
        $str .= "<td>$this->attendance</td>";
        $str .= "<td>$this->averagescores</td>";
        return $str;
	}
}
?>