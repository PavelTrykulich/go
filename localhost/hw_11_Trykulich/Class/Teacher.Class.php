<?php


class Teacher extends Person{
	public $subject = '';
	public function __construct($name,$grouphuman,$email,$phone,$subject){
		parent::__construct($name,$grouphuman,$email,$phone);
		$this->subject = $subject;
	}
	public function getVisitCard(){
		$str = '';
		$str = parent::getVisitCard();
        $str .= "<td></td><td></td><td>$this->subject</td>";
        return $str;
	}
}

?>