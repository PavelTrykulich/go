<?php
require 'peoples.array.php';
require 'Class/Person.Class.php';
require 'Class/Teacher.Class.php';
require 'Class/Admin.Class.php';
require 'Class/Student.Class.php';
require_once 'connection_db.php';

try{
  $sql = 'SELECT * from members';
  $result = $pdo->query($sql);

}catch(Exception $e){
  echo "Не удалось получить данные!";
  die();
}

$resultArray = $result->fetchAll();

$arrayObjectsTeachers = [];//create array objects teachers
$arrayObjectsAdmins = [];//create array objects teachers
$arrayObjectsStudendts = [];//create array objects teachers

foreach($resultArray as $people){
if($people['grouphuman'] == 'teacher'){
  $arrayObjectsTeachers[] = new Teacher($people['name'],
                                     $people['grouphuman'],
                                     $people['email'],
                                     $people['phone'],
                                     $people['subject']);
}
elseif($people['grouphuman'] == 'admin'){
  $arrayObjectsAdmins[] = new Admin($people['name'],
                                  $people['grouphuman'],
                                  $people['email'],
                                  $people['phone'],
                                  $people['workday']);
}
else{
  $arrayObjectsStudendts[] = new Student($people['name'],
                                       $people['grouphuman'],
                                       $people['email'],
                                       $people['phone'],
                                       $people['attendance'],
                                       $people['averagescores']);
}
}


?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Peopels</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Header block start-->
<div>
    <? include "header.html";?>
</div>
<!--Header block end-->


<!--Content blocck start-->
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Grouphuman</th>
      <th scope="col">Email</th>
      <th scope="col">Phone</th>
      <th scope="col">Attendance</th>
      <th scope="col">Averagescores</th>
      <th scope="col">Subject</th>
      <th scope="col">Workday</th>
    </tr>
  </thead>
     
  <tbody>
    
    <?php 
      for($i=0;$i<count($arrayObjectsStudendts);$i++):?>
        <tr>
         <?=$arrayObjectsStudendts[$i]->getVisitCard();?>
         <?php endfor;?>
        <tr>

    <?php 
      for($i=0;$i<count($arrayObjectsTeachers);$i++):?>
        <tr>
         <?=$arrayObjectsTeachers[$i]->getVisitCard();?>
         <?php endfor;?>
        <tr>

    <?php 
      for($i=0;$i<count($arrayObjectsAdmins);$i++):?>
        <tr>
         <?=$arrayObjectsAdmins[$i]->getVisitCard();?>
         <?php endfor;?>
        <tr>          
  </tbody>
</table>
<!--Content blocck end-->
    
<!--Footer block start-->
<div>
    <? include "footer.html";?>
</div>
<!--Footer block end-->

</body>
</html>