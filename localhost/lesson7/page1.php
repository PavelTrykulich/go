<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$_SESSION['text'] = 'Super secret text';

$_SESSION['userData'] = [
	'firstName' => 'John',
	'lastName' => 'Doe'
];

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<a href="page1.php">Setting session</a>
<a href="page2.php">Checking session</a>
<a href="page3.php">Deleting text element from session</a>
<a href="page4.php">DESTROY all session</a>
</body>
</html>
