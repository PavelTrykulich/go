<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$firstName = 'John';
$lastName = 'Doe';

function sayHello($userFirstName = '', $userLastName = '123'){

	$text = 'Hello, ' . $userFirstName . ' ' . $userLastName . '! Glad to see you! Have a nice day!<br>';

	return $text;

}

function checkAge($age, $firstName, $lastName = ''){
	$resultText = '';

	if($age == 1){
		return 'Are you serious?';
	}

	if($age < 18){
		$resultText = 'Sorry, come back later!';
	}else{
		$resultText = sayHello($firstName, $lastName);
	}

	return $resultText;
}


echo sayHello($firstName);
echo sayHello($firstName, 'Doe');
echo sayHello( '', 'Doe');
echo sayHello();

echo checkAge(18, 'John');


