<?php

define('USD', 26.2);
define('EUR', 30.1);
define('RUB', 0.3);

function convert($uah, $usd = USD, $eur = EUR, $rub = RUB){
	$result = [];

	$result['usd'] = round($uah / $usd, 2);
	$result['eur'] = round($uah / $eur, 2);
	$result['rub'] = round($uah / $rub, 2);

	return $result;
}


$price = convert(2500);

echo $price['usd']. ' ' . $price['eur'] . ' ' . $price['rub'];