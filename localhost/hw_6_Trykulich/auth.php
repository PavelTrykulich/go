<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Все учетные данные</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Heder block start-->
 <div>
    <? include "header.html";?>
 </div>
<!--Heder block end-->

<!-- Content block start -->

<?php 
  $surname = htmlspecialchars($_POST['surname'], ENT_QUOTES, 'UTF-8');
  $firstName = htmlspecialchars($_POST['fiestName'], ENT_QUOTES, 'UTF-8');
  $lastName = htmlspecialchars($_POST['lastName'], ENT_QUOTES, 'UTF-8');
  $status = htmlspecialchars($_POST['status'], ENT_QUOTES, 'UTF-8');
  $years = htmlspecialchars($_POST['years'], ENT_QUOTES, 'UTF-8');
  $commentary = htmlspecialchars($_POST['commentary'], ENT_QUOTES, 'UTF-8');?>
<div>
   <div class="alert alert-primary" role="alert">
   <?php if($_POST['years']>=18):?>

   <?php if($_POST['status']=='преподаватель'):  // translate into a generic case
   $text='преподавателей.';

   echo "Поздравляем. Вы успешно зарегистрированы и добавлены в список  " . $text;
     
   else:  
     
   echo "Поздравляем, Вы успешно зарегистрированы и добавлены в список  " . $_POST['status'] . 'ов.'; // translate into a generic case
   endif?>
   </div>

   <table class="table">
  <tbody>
    <tr>
      <th scope="row">Фамилия</th>
      <td><?=$_POST['surname']?></td>
    </tr>
    <tr>
      <th scope="row">Имя:</th>
      <td><?=$_POST['firstName']?></td>
    </tr>
    <tr>
      <th scope="row">Отчество:</th>
      <td><?=$_POST['lastName']?></td>
    </tr>
    <tr>
      <th scope="row">Возраст:</th>
      <td><?=$_POST['years']?></td>
    </tr>
    <tr>
      <th scope="row">Статус:</th>
      <td><?=$_POST['status']?></td>
    </tr>
    <tr>
      <th scope="row">Коментарии:</th>
      <td><?=$_POST['commentary']?></td>
    </tr>
  </tbody>
</table>

<?php else: ?>
     <div class=" text-danger " role="alert">
         <p class="text-danger"><h1><center>Извините вам меньше 18 лет</center></h1></p>
     </div>
    <?php endif; ?>
</div>

<!--Content block end-->

<!--Footer block start-->
    <div >
    <? include "footer.html";?>
    </div>
<!--Footer block end-->
</body>
</html>