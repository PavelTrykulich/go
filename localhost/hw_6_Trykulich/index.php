<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Все учетные данные</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Heder block start-->
 <div>
    <? include "header.html";?>
 </div>
<!--Heder block end-->

<!-- Content block start -->
<form action='auth.php' method='post'>
  <div class="form-row">
    
    <div class="col-md-4 mb-3">
      <label for="surname">Фамилия</label>
      <input type="text" name="surname" class="form-control" id="surname" placeholder="First name" value="Sterling" required>
    </div>

    <div class="col-md-4 mb-3">
      <label for="validationDefault01">Имя</label>
      <input type="text" name="firstName" class="form-control" id="firstName" placeholder="First name" value="Mark" required>
    </div>

    <div class="col-md-4 mb-3">
      <label for="lastName">О́тчество </label>
      <input type="text" name="lastName" class="form-control" id="lastName" placeholder="Last name" value="Otto" required>
    </div>
 
  <div class="form-group col-md-2">
      <label for="years">Возраст</label>
      <input type="text" name="years" class="form-control" id="years">
    </div>
  
  <div class="form-group" >
    <label for="status">Статус</label>
    <select class="custom-select" name="status" required>
      <option value="студент">Студент</option>
      <option value="администратор">Администратор</option>
      <option value="преподаватель">Преподаватель</option>
      
    </select>
    
  </div>

  <div class="form-group">
    <label for="commentary">Коментарий</label>
    <textarea class="form-control" id="commentary" name="commentary" rows="1"></textarea>
  </div>

  </div>
  <center><button class="btn btn-primary" type="submit">Регистрация </button></center>
</form>
<!--Content block end-->

<!--Footer block start-->
    <div>
    <? include "footer.html";?>
    </div>
<!--Footer block end-->
</body>
</html>