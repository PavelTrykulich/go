<?php
 
  $peoples = [['name' => 'Ethan O’Connor',
               'group' => 'student',
               'email' => 'Ethan@mail.com',
               'phone' => '123-456-78'
              ],
              ['name' => 'Dustin Horn',
               'group' => 'teacher',
               'email' => 'Dustin@mail.com',
               'phone' => '313-456-78'
              ],
              ['name' => 'Gary Bell',
               'group' => 'student',
               'email' => 'Gary@mail.com',
               'phone' => '985-436-98'
              ],
              ['name' => 'Wilfred King',
               'group' => 'admin',
               'email' => 'King@mail.com',
               'phone' => '653-356-23'
              ],
              ['name' => 'Mark Carson',
               'group' => 'admin',
               'email' => 'Mark@mail.com',
               'phone' => '852-451-79'
              ],
              ['name' => 'Leslie Greer',
               'group' => 'teacher',
               'email' => 'Leslie@mail.com',
               'phone' => '555-555-55'
              ],
              ['name' => 'Robert Daniels',
               'group' => 'student',
               'email' => 'Robert@mail.com',
               'phone' => '122-543-21'
              ],
              ['name' => 'William Wilkerson',
               'group' => 'teacher',
               'email' => 'William@mail.com',
               'phone' => '908-765-56'
              ],
              ['name' => 'Tyrone McBride',
               'group' => 'student',
               'email' => 'Tyrone@mail.com',
               'phone' => '456-456-45'
              ],
              ['name' => 'Thomas Logan',
               'group' => 'admin',
               'email' => 'Thomas@mail.com',
               'phone' => '999-456-23'
              ],
              ['name' => 'Baldric Young',
               'group' => 'teacher',
               'email' => 'Baldric@mail.com',
               'phone' => '319-316-54'
              ]
                

  ];
?>