<?php
require 'peoples.array.php';
require 'Class/Person.Class.php';
require 'Class/PersonVisitCart.Class.php';
$humans = [];

foreach($peoples as $people){
  $humans[] = new Person($people['name'],
                         $people['group'],
                         $people['email'],
                         $people['phone']);
  
  }
$personVisitCart = new PersonVisitCart();
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Peopels</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Header block start-->
<div>
    <? include "header.html";?>
</div>
<!--Header block end-->


<!--Content blocck start-->
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Group</th>
      <th scope="col">Email</th>
      <th scope="col">Phone</th>
    </tr>
  </thead>

  <tbody>
    <?php foreach ($humans as $human): ?>  
    <tr>      
      <?=$personVisitCart->writeVisitCard($human);?>    
    </tr>
    <?php endforeach;?>
  </tbody>
</table>
<!--Content blocck end-->

<!--Footer block start-->
<div>
    <? include "footer.html";?>
</div>
<!--Footer block end-->

</body>
</html>