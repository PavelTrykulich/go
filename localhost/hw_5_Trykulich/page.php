<?php include "peoples.php"; 
$i = 1;
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Cтуденты</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>

<!--Heder block start-->
 <div>
    <? include "header.html";?>
 </div>
<!--Heder block end-->

<!-- Content block start -->

<div>
<p><h2 class="text"><center>Cтуденты</center></h2></p>
<table class="table">
      <thead class="table-primary">
       <tr>
          <th scope="col">№</th>
          <th scope="col"><h5>Имя</h5></th>
          <th scope="col"><h5>Группа</h5></th>
          <th scope="col"><h5>Email</h5></th>
          <th scope="col"><h5>Номер</h5></th>
       </tr>
        </thead>
        <tbody class="font-weight-bold">     
                      
                     <?php 
                        foreach ($peoples as $key => $value):?> 
                                                 
                             <?php if($value['group'] == 'student'): ?>
                             <tr <?php //use if for change the color of the rows
                              if($key % 2 != 0):?>
                              class="table-secondary"
                              <?php endif ?>   
                             >                            
                             <td><?=$i++?></td>
                             <td><?=$value['name']?></td>
                             <td><?=$value['group']?></td>
                             <td><?=$value['email']?></td>
                             <td><?=$value['phone']?></td>    
                             </tr>
                             <?php endif ?>
                             </tr> 
                             <?php endforeach?> 
                                                                                            
         </tbody>
    </table>
</div>  
<!--Content block end-->

<!--Footer block start-->
    <div>
    <? include "footer.html";?>
    </div>
<!--Footer block end-->
</body>
</html>