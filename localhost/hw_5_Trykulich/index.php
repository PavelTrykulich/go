<?php include "peoples.php";
$i = 1;
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Все учетные данные</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Heder block start-->
 <div>
    <? include "header.html";?>
 </div>
<!--Heder block end-->

<!-- Content block start -->
<div>
<p><h2 class="text"><center>Все учетные данные</center></h2></p>
<table class="table">
      <thead class="table-primary">
       <tr>
          <th scope="col">№</th>
          <th scope="col"><h5>Имя</h5></th>
          <th scope="col"><h5>Группа</h5></th>
          <th scope="col"><h5>Email</h5></th>
          <th scope="col"><h5>Номер</h5></th>
       </tr>
        </thead>
        <tbody class="font-weight-bold">     
                      
                     <?php 
                        foreach ($peoples as $key => $value):?> 
                         <tr <?php //use goto and if for change the color of the rows
                         if($key % 2 != 0):?>
                              class="table-secondary"
                           <?php endif?>
                         
                         >                            
                             <td>
            <img src="<?=$value['picture'];  ?> " width="100" height="55"> 
     </td>                                      
                             <td><?=$value['name']?></td> 
                             <td><?=$value['group']?></td>
                             <td><?=$value['email']?></td>
                             <td><?=$value['phone']?></td>
                           </tr> 
                          <?php endforeach?>                                                             
         </tbody>
    </table>
</div>  
<!--Content block end-->

<!--Footer block start-->
    <div>
    <? include "footer.html";?>
    </div>
<!--Footer block end-->
</body>
</html>