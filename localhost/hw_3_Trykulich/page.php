<?php

  $name_EUR = 'EUR';               // start information about EUR
  $price_buy_EUR = 32.896;
  $price_sell_EUR = 33.571;
  $price_commercial_EUR = 33.2481;
  $price_NBU_EUR = 33.233394;      // end information about EUR

  $name_USD = 'USD';               // start information about USD
  $price_buy_USD = 26.812;
  $price_sell_USD = 27.188;
  $price_commercial_USD = 27.036;
  $price_NBU_USD = 27.071;         // end information about USD
  

  $UAH_sum = 1500;                            
  $UAH_in_EUR = 1500/$price_commercial_EUR;  //calculation to EUR
  $UAH_in_USD = 1500/$price_commercial_USD;  //calculation to USD

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Калькулятор</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	 <style>
   .table {
    width: 22%;
    margin: auto; 
    }
    .text {
    text-align: center;
    color: white;
   }
   .center{
   	text-align: center;
   }
   </style>	
</head>
<body>

<!--Heder block start-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    	<a href="#" class="navbar-brand">
    		<img src="images/dol.png" width="35" height="35" alt="logo">
    	</a>
    	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigations">
    		<span class="navbar-toggler-icon"></span>
    	</button> 
    	<div class="collapse navbar-collapse" id="navbarSupportedContent">
    		<ul class="navbar-nav mr-auto">
    			<li class="nav-item active">
    				<a href="index.php" class="nav-link">Главая</a>	
    			</li>
    			<li class="nav-item">			
    				<a href="page.php" class="nav-link">Kалькулятор</a>	
    			</li>
    			<li class="nav-item">
    				<a href="page2.php" class="nav-link">Контакты</a>	
    			</li>	
    		</ul>
			<form class="form-inline my-2 my-lg-0">
				<input type="text" class="form-control mr-sm-2" placeholder="Поиск" aria-label="Search">
				<button class="btn btn-outline-success my-2 my-sm-0 ">Поиск</button>
			</form>    		
    	</div>
 	</nav>
<!--Heder block end-->

<!-- Content block start -->
       	 <br>
		<table class="table center" width="10%">
  			<thead class="thead-white">
   		 <tr>
      		<th scope="col">Валюта</th>
      		<th scope="col">Деньги</th>
   		 </tr>
  			</thead>
 			 <tbody>
    		<tr>
     			 <th scope="row">
     			 	<select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
        				<option selected>UA</option>
        				<option value="1">USD</option>
        				<option value="2">EUR</option>
      				</select>
      				<td><input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Kоличество"></td>
     			 </th>
    		</tr>
   			 <tr>
    			  <th scope="row">
    			  	<select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
      				  <option selected>EUR</option>
        			  <option value="1">USD</option>
     				  <option value="2">UA</option>        
    			   </select>
    			  </th>
    			  <td><input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Kоличество">
    			  </td>	  
   			 </tr>
  				</tbody>
		</table>

    <div class="center">
		<button type="button" class="btn btn-primary">Kонвертировать</button>
	  </div>

    <div class="center">
      <h4>
      <!--use the rounding function to 2 -->
      
      <!--Output example start-->
      <?='Пример: ' . $UAH_sum . ' грн. - это ' . round($UAH_in_EUR,2) . ' € или ' .
      round($UAH_in_USD,2) . ' $.' ?>  
      <!--Output example end-->
      
      </h4>   
    </div>
	<p></p>
<!--Content block end-->

	<!--Footer block start-->
		<div id="footer" class="alert alert-dark center" role="alert">
			<span"> Trykulich Pasha HomeWork 3 (c)2018</span>
		</div>
	<!--Footer block end-->
</body>
</html>