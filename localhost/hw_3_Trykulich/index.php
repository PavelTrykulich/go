<?php

  $name_EUR = 'EUR';               // start information about EUR
  $price_buy_EUR = 32.896;
  $price_sell_EUR = 33.571;
  $price_commercial_EUR = 33.2481;
  $price_NBU_EUR = 33.233394;      // end information about EUR

  $name_USD = 'USD';               // start information about USD
  $price_buy_USD = 26.812;
  $price_sell_USD = 27.188;
  $price_commercial_USD = 27.036;
  $price_NBU_USD = 27.071;         // end information about USD

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Главная</title>
	<!--Bootsrap CSS-->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<style>
    .text{
    	text-align:  center;
    }
    .center{
   	text-align: center;
   }
  </style>
</head>
<body>

<!--Heder block start-->

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    	<a href="#" class="navbar-brand">
    		<img src="images/dol.png" width="35" height="35" alt="logo">
    	</a>
    	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigations">
    		<span class="navbar-toggler-icon"></span>
    	</button> 
    	<div class="collapse navbar-collapse" id="navbarSupportedContent">
    		<ul class="navbar-nav mr-auto">
    			<li class="nav-item active">
    				<a href="index.php" class="nav-link">Главая</a>	
    			</li>
    			<li class="nav-item">
    				<a href="page.php" class="nav-link">Kалькулятор</a>	
    			</li>
    			<li class="nav-item">
    				<a href="page2.php" class="nav-link">Контакты</a>	
    			</li>				
    		</ul>
			<form class="form-inline my-2 my-lg-0">
				<input type="text" class="form-control mr-sm-2" placeholder="Поиск" aria-label="Search">
				<button class="btn btn-outline-success my-2 my-sm-0">Поиск</button>
			</form>    		
    	</div>
 	</nav>
<!--Heder block end-->

<!-- Content block start -->
    <div>
	<p ><h2 class="text"> Курс валют в Украине</h2></p>	
		<table class="table">
  			<thead class="thead-dark">
   		 <tr>
      		<th scope="col"></th>
      		<th scope="col">Покупка</th>
      		<th scope="col">Продажа </th>
     		 <th scope="col">Коммерческий</th>
     		 <th scope="col">НБУ</th>
   		 </tr>
  			</thead>
 			 <tbody> 

    		 <tr>
     			  <th scope="row"><?=$name_EUR?></th>      
     			  <td><?=round($price_buy_EUR,4)?></td>    <!--use the rounding function to 4 -->
      		  <td><?=round($price_sell_EUR,4)?></td>
     			  <td><?=round($price_commercial_EUR,4)?></td>
     			  <td><?=round($price_NBU_EUR,4)?></td>
    		 </tr>

   			 <tr>
    			  <th scope="row"><?=$name_USD?></th> 
    			  <td><?=round($price_buy_USD,4)?></td>     <!--use the rounding function to 4 -->
     			  <td><?=round($price_sell_USD,4)?></td>
      		  <td><?=round($price_commercial_USD,4)?></td>
     			  <td><?=round($price_NBU_USD,4)?></td>
   			 </tr>

  				</tbody>
		</table>
	</div>
	<!--Content block end-->
  
	<!--Footer block start-->
		<div id="footer" class="alert alert-dark center " role="alert">
			<span>Trykulich Pasha HomeWork 3 (c) 2018</span>
		</div>
	<!--Footer block end-->

</body>
</html>