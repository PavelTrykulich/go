<?php

//contact details start
$number = '+380973332156'; 
$address = 'ул.Бунина 3';
$mail = 'pochta@gmail.com'; 
//contact details end

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Контакты</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<style>
    .text{
    	text-align:  center;
    }
    .center{
   	text-align: center;
   }
  </style>
</head>
<body>

<!--Heder block start-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    	<a href="#" class="navbar-brand">
    		<img src="images/dol.png" width="35" height="35" alt="logo">
    	</a>
    	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigations">
    		<span class="navbar-toggler-icon"></span>
    	</button> 
    	<div class="collapse navbar-collapse" id="navbarSupportedContent">
    		<ul class="navbar-nav mr-auto">
    			<li class="nav-item active">
    				<a href="index.php" class="nav-link">Главая</a>	
    			</li>
    			<li class="nav-item">
    				<a href="page.php" class="nav-link">Kалькулятор</a>	
    			</li>
    			<li class="nav-item">
    				<a href="page2.php" class="nav-link">Контакты</a>	
    			</li>
    		</ul>
			<form class="form-inline my-2 my-lg-0">
				<input type="text" class="form-control mr-sm-2" placeholder="Поиск" aria-label="Search">
				<button class="btn btn-outline-success my-2 my-sm-0">Поиск</button>
			</form>    		
    	</div>
 	</nav>
<!--Heder block end-->

<!-- Content block start -->
<h1 class="text">Контакты</h1>
<div class="alert alert-primary" role="alert">
  Телефон: <a href="#" class="alert-link"><?=$number?></a>
</div>
<div class="alert alert-primary" role="alert">
  Адрес: <a href="#" class="alert-link"><?=$address?></a>
</div>
<div class="alert alert-primary" role="alert">
  Эл. почта: <a href="#" class="alert-link"><?=$mail?></a>
</div

<h1 class="text">Обратная связь</h1>

<form>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationDefault01">Имя:</label>
      <input type="text" class="form-control" id="validationDefault01" placeholder="Иван"  required>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationDefault02">Фамилия:</label>
      <input type="text" class="form-control" id="validationDefault02" placeholder="Иванов"  required>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationDefaultUsername">Username</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroupPrepend2">@</span>
        </div>
        <input type="text" class="form-control" id="validationDefaultUsername" placeholder="Username" aria-describedby="inputGroupPrepend2" required>
      </div>
    </div>
    <input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Введите ваше сообщение.....">
  </div>
   <div class="center"><button class="btn btn-primary" type="submit">Отправить</button><p></p>
   </div>
</form>
<!--Content block end-->

<!--Footer block start-->
		<div id="footer" class="alert alert-dark center" role="alert">
			<span>Trykulich Pasha HomeWork 3 (c) 2018</span>
		</div>
	<!--Footer block end-->

</body>
</html>