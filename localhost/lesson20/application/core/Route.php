<?php
class Route{
    //http://l20.com/Portfolio/show
    static public function start(){
//     echo $_SERVER['REQUEST_URI'];
       $routes = explode('/', $_SERVER['REQUEST_URI']);

       $controllerName = 'Main';
       $actionName = 'index';

       if(!empty($routes[1])){
           $controllerName = $routes[1];
       }

       if(!empty($routes[2])){
           $actionName = $routes[2];
       }

       $controllerName = 'Controller_' .$controllerName;
       $actionName = 'action_' .$actionName;

       $controllerPath ='application/controllers/' . $controllerName . '.php';
       if(file_exists($controllerPath)){
           require_once $controllerPath;
       }else{
           Route::error404();
       }

       $controller = new $controllerName();
       if(method_exists($controller, $actionName)){
           $controller->$actionName();
       }else{
           Route::error404();
       }

    }

    static public function error404(){
         die('Page doesn\'t exist');
    }
}