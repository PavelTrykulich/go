<?php
	$product = [
	'title' => 'Macbook',
	'price' => 1100,
	'category' => 'laptop',
	'description' => 'Super laptop'
];

$students = ['Alex', 'John', 'Jack', 'Susie', 'Ben', 'Jannice'];
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php foreach($product as $key => $val):?>
		<div>
			<strong><?=$key;?></strong> - <?=$val?>
		</div>
	<?php endforeach;?>

	<?php for($x = 0; $x < count($students);$x++):?>
		<div>
			<strong><?=$students[$x]?></strong>
		</div>
	<?php endfor;?>

</body>
</html>