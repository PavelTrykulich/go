<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$students = ['Alex', 'John', 'Jack', 'Susie', 'Ben'];

$x = 1;
/*$x = $x + 1;
$x += 1;
$x++;
echo $x;

$x -= 2;*/


$x = 1;
// ==
// ===
// > , >=
// < , <=

while($x <= 10){

	echo $x.'<br>';
	$x++;

}

$x = 10;

while($x >= 0){
	echo $x.'<br>';
	$x--;
}

$students = ['Alex', 'John', 'Jack', 'Susie', 'Ben', 'Jannice'];
//5
$x = 0;
// echo count($students);
while($x < count($students)){
	echo $students[$x] . '<br>';
	$x++;
}

echo "<br>";

$y = 0;
do{
	echo $students[$y] . '<br>';
	$y++;
}while($y <= 1);

for($x = 1; $x <= 20; $x++){
	echo $x . "<br>";
}

$students = ['Alex', 'John', 'Jack', 'Susie', 'Ben', 'Jannice'];

for($x = 0;$x < count($students); $x++){
	echo $students[$x].'<br>';
	echo "Hello!";
}

echo '<br>';
echo '<br>';

$students = ['Alex', 'John', 'Jack', 'Susie', 'Ben', 'Jannice'];

foreach ($students as $student) {
	echo $student . '<br>';
}

$product = [
	'title' => 'Macbook',
	'price' => 1100,
	'category' => 'laptop',
	'description' => 'Super laptop'
];
echo "<br><br><br><br><br><br><br><br>";
echo "<pre>";
print_r($product);

foreach($product as $key => $value){
	// $key .= '_';
	$value .= ' awesome!';
	$product[$key] .= '++++';

	echo $value. ' | ' . $product[$key] . '<br>';
	// echo $key . ' - ' . $value . '<br>';
}

echo "<pre>";
print_r($product);