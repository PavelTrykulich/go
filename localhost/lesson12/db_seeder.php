<?php
require_once 'db_connection.php';

try{
	$sql = 'INSERT INTO quotes SET
			quote_text = "Php is awesome!",
			quote_date = CURDATE()
	';
	$pdo->exec($sql);
	echo "Test data added";
	die();

}catch(Exception $e){
	echo "Error adding test data" . $e->getMessage();
}