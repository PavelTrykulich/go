<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'db_connection.php';

try{
	$sql = 'SELECT * from quotes';
	// $sql = 'SELECT id,quote_text from quotes';
	$result = $pdo->query($sql);

}catch(Exception $e){
	echo "Не удалось получить данные!";
	die();
}

$resultArray = $result->fetchAll();
?>

<!DOCTYPE html>
<html>
<head>
	<title>All quotes</title>
</head>
<body>
	<div>
		<ul>
			<?php foreach($resultArray as $quote):?>
				<li>
					<strong><?=$quote['quote_text']?></strong>
					<span><?=$quote['quote_date']?></span>
				</li>
			<?php endforeach;?>
		</ul>
	</div>
</body>
</html>






