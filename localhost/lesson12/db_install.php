<?php
require_once 'db_connection.php';
try{
	$sql = 'CREATE TABLE quotes(
			id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			quote_text TEXT,
			quote_date DATE NOT NULL
	)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
	$pdo->exec($sql);

}catch(PDOException $e){
	echo 'Не удалось создать таблицу quotes ' . $e->getMessage();
}