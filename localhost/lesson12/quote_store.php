<?php
require_once 'db_connection.php';

if(!isset($_POST['quote_text'])){
	header('Location:quote_create.php');
	die();
}

$quote_text = htmlspecialchars($_POST['quote_text'], ENT_QUOTES, 'utf-8'); 

try{
	$sql = 'INSERT INTO quotes SET
			quote_text = "'.$quote_text.'",
			quote_date = CURDATE()
	';
	$pdo->exec($sql);
	header('Location:index.php');

}catch(Exception $e){
	echo "Error adding test data" . $e->getMessage();
}