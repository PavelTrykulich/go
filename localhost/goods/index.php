<?php
/**
 * Created by PhpStorm.
 * User: Pasha
 * Date: 05.05.2018
 * Time: 16:32
 */
require_once 'connection_db.php';
require_once 'Class\Publication.php';
require_once 'Class\News.php';
require_once 'Class\Article.php';
require_once 'Class\PublicationsWriter.php';

$arrArticle = new PublicationsWriter('Article',$pdo);
$arrNews = new PublicationsWriter('News',$pdo);

?>
<!DOCTYPE html>
<html>
<head>
    <title>Main</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>

<div>
    <? include "header.html";?>  <!--header block-->
</div>

<table class="table">
    <thead class="thead-light">
    <tr>
        <th width="200" scope="col">Title</th>
        <th width="200" scope="col">Type</th>
        <th width="200" scope="col">Introductory</th>
        <th width="200" scope="col">Author</th>
        <th width="200" scope="col">Source</th>
        <th width="200" scope="col">Read ALL</th>



    </tr>
    </thead>

    <tbody>
            <?=$arrArticle->write();?>
            <?=$arrNews->write();?>
    </tbody>
</table>

<div>
    <? include "footer.html";?>  <!--footer block-->
</div>

</body>
</html>