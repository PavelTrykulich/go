<?php
require_once 'connection_db.php';
include 'publicationsArray.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Add data from the array</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>


<!--Header block start-->
<div>
    <? include "header.html";?>
</div>
<!--Header block end-->

<?php
try{
	$sql = 'INSERT INTO portal SET

			title = :title,
			publicationType = :publicationType,
			introductory = :introductory,
			long_text = :long_text,
			author = :author,
			source_publication = :source_publication
	';
	
	$pdoStatement = $pdo->prepare($sql);

	foreach($publicationsArray as $value){
		$pdoStatement->bindValue(':title', $value['title']);
		$pdoStatement->bindValue(':publicationType', $value['publicationType']);
		$pdoStatement->bindValue(':introductory', $value['introductory']);
		$pdoStatement->bindValue(':long_text', $value['long_text']);
		$pdoStatement->bindValue(':author', $value['author']);
		$pdoStatement->bindValue(':source_publication', $value['source_publication']);
		$pdoStatement->execute();
	}


	echo "Data from array add";
	die();
	?>
	  
<?php
}catch(Exception $e){
	echo "Error add" . $e->getMessage();

}?>

<!--Footer block start-->
<div>
    <? include "footer.html";?>
</div>
<!--Footer block end-->
</body>
</html>