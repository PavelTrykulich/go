<?php
/**
 * Created by PhpStorm.
 * User: Pasha
 * Date: 06.05.2018
 * Time: 14:57
 */

class Publication
{
    protected $id = 0;
    protected $title = ' ';
    protected $publicationType = ' ';
    protected $introductory = ' ';
    protected $long_text = ' ';


    public function __construct($id,$title,$publicationType,$introductory,$long_text)
    {
        $this->id = $id;
        $this->title = $title;
        $this->publicationType = $publicationType;
        $this->introductory = $introductory;
        $this->long_text = $long_text;
    }

    /**
     * @param $id
     * @param PDO $pdo
     * @return Publication
     */
    static public function create($id, PDO $pdo){
        //get data from db
        $sql = 'SELECT * FROM portal WHERE id=:id';
        $pdoSt = $pdo->prepare($sql);
        $pdoSt->bindValue(':id', $id);
        $pdoSt->execute();
        $portalArr = $pdoSt->fetch();
        // create object
        $publication = new self($portalArr['id'],$portalArr['title'], $portalArr['publicationType'], $portalArr['introductory'],$portalArr['long_text']);
        // set id
        $publication->setId($portalArr['id']);
        return $publication;
    }
    public function setId($id){
        $this->id = $id;
    }
    public function getId(){
        return $this->id;
    }
    public function getTitle(){
        return $this->title;
    }
    public function getPublicationType(){
        return $this->publicationType;
    }
    public function getIntroductory(){
        return $this->introductory;
    }
    public function getLongText(){
        return $this->long_text;
    }


}