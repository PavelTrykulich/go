<?php

class PublicationsWriter
{
    protected $publications;

    public function __construct($publicationType, PDO $pdo)
    {
        $sql = 'SELECT * FROM portal WHERE publicationType=:publicationType ';
        $pdoSt = $pdo->prepare($sql);
        $pdoSt->bindValue(':publicationType', $publicationType);
        $pdoSt->execute();
        $portalArr = $pdoSt->fetchAll();

        foreach ($portalArr as $value){
            if ($value['publicationType'] == 'Article') {
                        $this->publications[]= new Article($value['id'],$value['title'], $value['publicationType'], $value['introductory'],
                        $value['long_text'], $value['author']);
            }
            if ($value['publicationType'] == 'News') {
                    $this->publications[]= new News($value['id'],$value['title'], $value['publicationType'], $value['introductory'],
                    $value['long_text'], $value['source_publication']);
            }
        }

    }

    public function getPublications(){
        return $this->publications;
    }

    public function write(){
        $str = ' ';

        foreach ($this->publications as $value){
                $str .= '<tr>' . '<td>'. $value->getTitle() .  '</td>';
                $str .= '<td>'. $value->getPublicationType() .  '</td>';
                $str .= '<td>'. $value->getIntroductory() .  '</td>';

                if($value->getPublicationType()=='Article'){
                    $str .= '<td>'. $value->getAuthor() .  '</td>';
                    $str .= '<td></td>';}

                if($value->getPublicationType()=='News'){
                    $str .= '<td></td>';
                    $str .= '<td>'. $value->getSoursePublication() . '</td>';}

                    $e=$value->getId(); //get id fot URL
                    $str .= '<td>' . "<a href='portal_show.php?id=$e'>All Information</a>" . '</td>';
                    $str .= '</tr>';

        }
        return $str;

    }



}
