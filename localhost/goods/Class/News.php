<?php
/**
 * Created by PhpStorm.
 * User: Pasha
 * Date: 06.05.2018
 * Time: 15:21
 */

class News extends Publication
{
    protected $source_publication = ' ';

    function __construct($id,$title, $publicationType, $introductory, $long_text, $source_publication)
    {
        parent::__construct($id,$title, $publicationType, $introductory, $long_text);
        $this->source_publication = $source_publication;
    }
        public function getSoursePublication(){
        return $this->source_publication;
    }
}