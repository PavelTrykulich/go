<?php
require_once 'connection_db.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Create table</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Header block start-->
<div>
    <? include "header.html";?>
</div>
<!--Header block end-->

<?php

try{
	$sql = 'CREATE TABLE portal(
			id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			title TEXT,
			publicationType TEXT,
			introductory TEXT,
			long_text TEXT,
			author TEXT,
			source_publication TEXT
	)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
	$pdo->exec($sql);

}catch(PDOException $e){
	echo 'Cant create table' . $e->getMessage();
}?>

<!--Footer block start-->
<div>
    <? include "footer.html";?>
</div>
<!--Footer block end-->
</body>
</html>