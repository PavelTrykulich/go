<?php
$product1 = [
	'title' => 'Macbook',
	'price' => 1000,
	'category'=> 'laptop',
];
$product2 = [
	'title' => 'Iphone',
	'price' => 500,
	'category'=> 'phone',
];
$product3 = [
	'title' => 'LG',
	'price' => 800,
	'category'=> 'laptop',
];

$product1['img'] = 'macbook1.jpg';
$product1['title'] = 'Macbook Air';
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Lesson3</title>
</head>
<body>
	<table>
		<tr>
			<th>title</th>
			<th>price</th>
			<th>category</th>
		</tr>

		<tr>
			<td><?php echo $product1['title'];?></td>
			<td> <?=$product1['price'];?> </td>
			<td> <?=$product1['category'];?> </td>
		</tr>

		<tr>
			<td> <?=$product2['title'];?></td>
			<td> <?=$product2['price'];?> </td>
			<td> <?=$product2['category'];?> </td>
		</tr>

		<tr>
			<td> <?=$product3['title'];?></td>
			<td> <?=$product3['price'];?> </td>
			<td> <?=$product3['category'];?> </td>
		</tr>

	</table>
</body>
</html>

