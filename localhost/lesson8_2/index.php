<?php

$cookieName = 'check'; // name of cookie
$cookieValue = 'Super information'; // value of cookie
$cookieExprire = time() + 60 * 60 * 24 * 2; // end on cookie life

setcookie($cookieName, $cookieValue, $cookieExprire);

$site_counter = $_COOKIE['site_counter'];
if(isset($site_counter)){
	$site_counter++;
	setcookie('site_counter', $site_counter, time()+ 60*60);
}else{
	setcookie('site_counter', 1, time()+ 60*60);
}


echo "You have reviewed this page " . $_COOKIE['site_counter']. ' times ';