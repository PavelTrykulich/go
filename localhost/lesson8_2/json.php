<?php
$student = [
	'name' => 'Jack',
	'courses' => [
		'php' => 5,
		'html' => 4
	],
	'marks' => [5,4,3,4,5]
];

$jsonString = json_encode($student);

setcookie('student', $jsonString, time() + 60 * 60 * 3);

echo $jsonString;

