<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('USD', 26.2);

$products = [
	[
		'title' => 'Potato',
		'category' => 'vegetable',
		'price' => 270.26
	],
	[
		'title' => 'Apple',
		'category' => 'fruit',
		'price' => 200
	],
	[
		'title' => 'Banana',
		'category' => 'fruit',
		'price' => 250
	],
	[
		'title' => 'CocaCola',
		'category' => 'drink',
		'price' => 127
	],
	[
		'title' => 'Redbull',
		'category' => 'drink',
		'price' => 232
	],

];

foreach($products as $product){
	if($product['category'] == 'vegetable'){

		$product['price'] *= 0.9;
		$product['price'] = round($product['price'], 2);

	}elseif($product['category'] == 'drink'){

		$product['price'] *= 0.7;`
		$product['price'] = round($product['price'], 2);

	}elseif ($product['category'] == 'fruit'){
		$product['price'] *= 0.8;
		$product['price'] = round($product['price'], 2);
	}

	




	echo $product['title'] . ' - ' . $product['price']. '<br>';
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>miniShop</title>
</head>
<body>
	<?php foreach($products as $product):?>
		<?php if($product['category'] == 'fruit'):?>
			<div>
				<h2><?=$product['title'];?></h2>
				<p><?=$product['price'];?></p>
			</div>
		<?php endif;?>
	<?php endforeach;?>
</body>
</html>