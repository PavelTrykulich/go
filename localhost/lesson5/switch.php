 <?php

 $products = [
	[
		'title' => 'Potato',
		'category' => 'vegetable',
		'price' => 270.26
	],
	[
		'title' => 'Apple',
		'category' => 'fruit',
		'price' => 200
	],
	[
		'title' => 'Banana',
		'category' => 'fruit',
		'price' => 250
	],
	[
		'title' => 'CocaCola',
		'category' => 'drink',
		'price' => 127
	],
	[
		'title' => 'Redbull',
		'category' => 'drink',
		'price' => 232
	],
	[
		'title' => 'Chiken',
		'category' => 'meat',
		'price' => 11.353
	],
	[
		'title' => 'Salmon',
		'category' => 'fish',
		'price' => 2.333
	],

];

foreach ($products as $product) {
	switch($product['category']){
		case 'fish':
		case 'vegetable':
			$product['price'] *= 0.9;
			$product['price'] = round($product['price'], 2);
			break;
		case 'fruit':
			$product['price'] *= 0.8;
			$product['price'] = round($product['price'], 2);
			break;
		case 'drink':
			$product['price'] *= 0.7;
			$product['price'] = round($product['price'], 2);
			break;

		default:
			$product['price'] = round($product['price'], 2);
			break;
	}
}
