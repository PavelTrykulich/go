<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require 'ShopProduct.Class.php';
require 'ShopProductWriter.Class.php';

$shopProduct1 = new ShopProduct('Php is awesome', 'John', 'Doe', 30);
$shopProduct2 = new ShopProduct('Html is awesome', 'Jack', 'Jones', 21);

$shopProductWriter = new ShopProductWriter();

echo $shopProductWriter->write($shopProduct1);
echo "<br>";
echo $shopProductWriter->write($shopProduct2);

class Wrong{};
$wrongObject = new Wrong();
echo "<br>";
// echo $shopProductWriter->write($wrongObject);
// echo $shopProductWriter->write('');
echo $shopProductWriter->write(NULL);

