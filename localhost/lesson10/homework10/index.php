<?php
session_start();
require 'data.array.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Products</title>
</head>
<body>
<h1>products:</h1>

<?php foreach($products as $key => $product):?>
	<div>
		<a href="cart_add.php?key=<?=$key?>"><?=$product['title']?></a>
	</div>
<?php endforeach;?>


<?php if(isset($_SESSION['cart'])):?>
<div>
	<h2>Cart:</h2>
	<?php foreach($_SESSION['cart'] as $productKey => $productAmount):?>
		<?=$products[$productKey]['title'];?> x <?=$productAmount;?>
	<?php endforeach;?>
</div>
<?php endif;?>

</body>
</html>