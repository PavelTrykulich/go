<?php
session_start();
if(!isset($_GET['key'])){
	header('Location:index.php');
	die();
}
$cart = [];
$key = $_GET['key'];
if(isset($_SESSION['cart'])){
	$cart = $_SESSION['cart'];
}

if(isset($cart[$key])){
	$cart[$key]++;
}else{
	$cart[$key] = 1;
}

$_SESSION['cart'] = $cart;

header('Location:index.php');
die();

