<?php 
require 'data.array.php';
require 'functions.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php foreach($products as $product):?>
		<div>
			<h2><?=$product['title'];?></h2>
			<p>price: <?=countPrice($product['price_type'],$product['price_val'],$product['discount_type'],$product['discount_val'], $currencies)?></p>
		</div>
	<?php endforeach;?>
</body>
</html>
