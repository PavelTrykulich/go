<?php

$currencies = [
	'uah' => [
		'name' => 'Гривна',
		'value' => 1
	],
	'usd' => [
		'name' => 'Доллар',
		'value' => 26.2
	],
	'eur' => [
		'name' => 'Евро',
		'value' => 30
	],
];

$products = [
	[
		'title' => 'Macbook',
		'price_type' => 'usd',
		'price_val' => 1000,
		'discount_type' => 'percent',
		'discount_val' => 10,

	],
	[
		'title' => 'Iphone',
		'price_type' => 'eur',
		'price_val' => 400,
		'discount_type' => 'value',
		'discount_val' => 30,

	],
	[
		'title' => 'Lenovo',
		'price_type' => 'uah',
		'price_val' => 400,
		'discount_type' => 'percent',
		'discount_val' => 20,

	]
];