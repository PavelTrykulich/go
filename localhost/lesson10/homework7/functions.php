<?php
function convert($basicPrice, $currency){
	return round($basicPrice * $currency, 2);
}

function discount($basicPrice, $discountType, $discountValue){

	if($discountType == 'value'){
		return round($basicPrice - $discountValue, 2);
	}else{
		$basicPrice -= $basicPrice * $discountValue / 100;
		return round($basicPrice, 2);
	}

}

function countPrice($priceType, $priceValue, $discountType, $discountValue, $currencies ){
	$finalPrice = $priceValue;
	if($priceType != 'uah'){
		$finalPrice = convert($priceValue, $currencies[$priceType]['value']);
	}
	$finalPrice = discount($finalPrice, $discountType, $discountValue);
	return $finalPrice;
}