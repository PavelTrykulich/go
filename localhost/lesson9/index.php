<?php
require 'ShopProduct.Class.php';
require 'products.array.php';

$productsObjects = [];
foreach($products as $product){
	$productsObjects[] = new ShopProduct($product['title'],
										 $product['authorFirstName'],
										 $product['authorLastName'],
										 $product['price']);
}
?>


<!DOCTYPE html>
<html>
<head>
	<title>Book shop</title>
	<style type="text/css">
		table,td,th{
			border: 1px dotted black;
		}
	</style>
</head>
<body>

	<table>
		
		<tr>
			<th>title</th>
			<th>author</th>
			<th>price</th>
		</tr>

		<?php foreach($productsObjects as $product): ?>

			<tr>
				<td><?=$product->title?></td>
				<td><?=$product->fullName()?></td>
				<td><?=$product->price?></td>
			</tr>

		<?php endforeach;?>

	</table>

</body>
</html>