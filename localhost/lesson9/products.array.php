<?php

$products = [
	[
		'title' => 'Php is awesome!',
		'authorFirstName' => 'John',
		'authorLastName' => 'Doe',
		'price' => 30
	],
	[
		'title' => 'We love Laravel5!',
		'authorFirstName' => 'Jim',
		'authorLastName' => 'Colombo',
		'price' => 21
	],
	[
		'title' => 'Smth about HTML',
		'authorFirstName' => 'Jack',
		'authorLastName' => 'McDonald',
		'price' => 17
	],
];