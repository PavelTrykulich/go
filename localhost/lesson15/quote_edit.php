<?php
if(!isset($_GET['id'])){
	header('Location:index.php');
}
require_once 'db_connection.php';
require_once 'Quote.Class.php';

$id = (int)$_GET['id'];
$quote = Quote::getQuoteById($id, $pdo);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit</title>
</head>
<body>
	<form action='quote_update.php' method="post">
		<label for='quote_text'>Enter quote text:</label>
		<br>
		<textarea name='quote_text' id="quote_text"><?=$quote->getText()?></textarea>
		<br>
		<label for="quote_date">Enter date:</label>
		<br>
		<input type="date" name="quote_date" id="quote_date" value="<?=$quote->getDate()?>">
		<br>
		<label for='quote_full_text'>Enter Full Text:</label>
		<br>
		<textarea name="quote_full_text" id='quote_full_text'><?=$quote->getFullText()?></textarea>
		<input type="hidden" name="id" value='<?=$quote->getId()?>' >
		<button type='submit'>Save</button>
	</form>
</body>
</html>
