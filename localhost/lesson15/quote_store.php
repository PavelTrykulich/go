<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'db_connection.php';
require_once 'Quote.Class.php';

if(!isset($_POST['quote_text'])){
	header('Location:quote_create.php');
	die();
}

$quote_text = htmlspecialchars($_POST['quote_text'], ENT_QUOTES, 'utf-8'); 
$quote_date = htmlspecialchars($_POST['quote_date'], ENT_QUOTES, 'utf-8'); 
$quote_full_text = htmlspecialchars($_POST['quote_full_text'], ENT_QUOTES, 'utf-8');

$quote = new Quote($quote_text, $quote_date, $quote_full_text);
$quote->store($pdo);
header('Location:index.php');
die();