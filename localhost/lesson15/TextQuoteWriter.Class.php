<?php 
class TextQuoteWriter extends QuoteWriter{
	public function write(){
		$str = '';
		foreach($this->quotes as $quote){
			$str.= 'Text: ' . $quote -> getText();
			$str.= ' Date: ' . $quote -> getDate();
		}
		return $str;
	}
}