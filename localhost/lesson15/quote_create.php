<!DOCTYPE html>
<html>
<head>
	<title>Add a quote</title>
</head>
<body>
	<h1>Add a quote</h1>

	<div>
		<form action='quote_store.php' method='POST'>
			<label for='quote_text'>Enter quote text:</label>
			<br>
			<textarea name='quote_text' id="quote_text"></textarea>
			<br>
			<label for="quote_date">Enter date:</label>
			<br>
			<input type="date" name="quote_date" id="quote_date">
			<br>
			<label for='quote_full_text'>Enter quote full text:</label>
			<br>
			<textarea name='quote_full_text' id="quote_full_text"></textarea>
			<br>

			<button type='submit'>Save</button>
		</form>
	</div>

</body>
</html>