<?php
if(!isset($_GET['id'])){
	header('Location:index.php');
}
require_once 'db_connection.php';

try{
	$id = (int)$_GET['id'];
	$sql = "SELECT id, quote_text FROM quotes WHERE id = :id";
	$x = $pdo->prepare($sql);
	$x->bindValue(':id', $id);
	$x->execute();
	$quote = $x->fetch();
}catch(Exception $e){
	echo 'Error'.$e->getMessage();
	die();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Delete</title>
</head>
<body>
	<h1>Do you realy want to delete <?=$quote['quote_text']?>?</h1>
	<form action="quote_destroy.php" method="post">
		<input type="hidden" name="id" value='<?=$quote['id']?>'>
		<button type='submit'>Yes</button>
	</form>
</body>
</html>