<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'db_connection.php';
require_once 'Quote.Class.php';
require_once 'QuoteWriter.Class.php';
require_once 'TextQuoteWriter.Class.php';
$quote1 = Quote::getQuoteById(1, $pdo);
$quote2 = Quote::getQuoteById(2, $pdo);
$quote3 = Quote::getQuoteById(3, $pdo);

$textQuoteWriter = new TextQuoteWriter();
$textQuoteWriter->addQuote($quote1);
$textQuoteWriter->addQuote($quote2);
$textQuoteWriter->addQuote($quote3);
echo $textQuoteWriter->write();