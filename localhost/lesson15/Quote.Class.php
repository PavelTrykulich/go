<?php
class Quote{
	protected $id = 0;
	protected $text = '';
	protected $date = '';
	protected $fullText = '';

	public function __construct($text, $date, $fullText){
		$this->text = $text;
		$this->date = $date;
		$this->fullText = $fullText;
	}

	static public function getQuoteById($id, PDO $pdo){
		//Получаем данные из бд
		$sql = 'SELECT * FROM quotes WHERE id=:id';
		$pdoSt = $pdo->prepare($sql);
		$pdoSt->bindValue(':id', $id);
		$pdoSt->execute();
		$quoteArr = $pdoSt->fetch();
		// Создаем объект 
		$quote = new self($quoteArr['quote_text'], $quoteArr['quote_date'], $quoteArr['quote_full_text']);
		// При помощи сеттера уставливаем id
		$quote->setId($quoteArr['id']);
		return $quotes;
	}

	public function store(PDO $pdo, $id = 0){
		$sql = '';
		if(!$id){
			$sql = 'INSERT INTO quotes SET
			quote_text = :quote_text,
			quote_date = :quote_date,
			quote_full_text = :quote_full_text';
		}else{
			$sql = 'UPDATE quotes 
				SET quote_text = :quote_text,
				 quote_full_text = :quote_full_text,
				 quote_date = :quote_date
				 WHERE id ="'. $id . '"';
		}
		$x = $pdo->prepare($sql);
		$x->bindValue(':quote_text', $this->text);
		$x->bindValue(':quote_date', $this->date);
		$x->bindValue(':quote_full_text', $this->fullText);
		$x->execute();
		if(!$id){
			$this->setId($pdo->lastInsertId());
		} 

	}

	public function getText(){
		return $this->text;
	}

	public function getDate(){
		return $this->date;
	}

	public function getFullText(){
		return $this->fullText;
	}

	public function getId(){
		return $this->id;
	}

		public function setId($id){
			$this->id = $id;
		}
}