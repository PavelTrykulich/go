<?php
if(!isset($_GET['id'])){
	header('Location:index.php');
}
require_once 'db_connection.php';
require_once 'Quote.Class.php';

$id = (int)$_GET['id'];
$quote = Quote::getQuoteById($id, $pdo);

?>
<!DOCTYPE html>
<html>
<head>
	<title><?=$quote->getText()?></title>
</head>
<body>
	<h1><?=$quote->getText()?></h1>
	<p><?=$quote->getFullText()?></p>
	<br>
	<a href="index.php">Back</a>
</body>
</html>