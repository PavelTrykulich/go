<?php
require_once 'db_connection.php';

$quotes = [
	[
		'quote_text' => 'Php is awesome',
		'quote_date' => '2018-04-02',
		'quote_full_text' => 'Php is awesome'
	],
	[
		'quote_text' => 'HTML is awesome',
		'quote_date' => '2018-04-03',
		'quote_full_text' => 'HTML is awesome'
	],
	[
		'quote_text' => 'CSS is awesome',
		'quote_date' => '2018-04-04',
		'quote_full_text' => 'CSS is awesome'
	],
];

try{
	$sql = 'INSERT INTO quotes SET
			quote_text = :quote_text,
			quote_date = :quote_date,
			quote_full_text = :quote_full_text
	';
	$pdoStatement = $pdo->prepare($sql);

	foreach($quotes as $quote){
		$pdoStatement->bindValue(':quote_text', $quote['quote_text']);
		$pdoStatement->bindValue(':quote_date', $quote['quote_date']);
		$pdoStatement->bindValue(':quote_full_text', $quote['quote_full_text']);
		$pdoStatement->execute();
	}


	echo "Test data added";
	die();

}catch(Exception $e){
	echo "Error adding test data" . $e->getMessage();
}