<?php
	 class QuoteWriter{
	protected $quotes = [];

	public function addQuote(Quote $quote){
		$this->quotes[] = $quote;
	}

	abstract public function write();
}