<?php
require_once 'db_connection.php';
require_once 'Quote.Class.php';

$quote_text = $_POST['quote_text'];
$quote_full_text = $_POST['quote_full_text'];
$quote_date = $_POST['quote_date'];
$id = $_POST['id'];

$quote = new Quote($quote_text, $quote_date, $quote_full_text);
$quote->store($pdo, $id);
header('Location:index.php');