<?php 
class StaticClass{
	const SUPERCONSTANT = 'super value';

	static public $name = 'John Doe';

	static public function sayHello(){
		return 'Hello ' . self::$name . '!';
	}
}