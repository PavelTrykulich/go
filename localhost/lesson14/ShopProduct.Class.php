<?php
class ShopProduct{
	protected $title = '';
	protected $authorFirstName = '';
	protected $authorLastName = '';
	protected $price = 0;
	protected $discount = 0;

	public function __construct($title, $firstName, $lastName, $price, $discount){
		$this->title = $title;
		$this->authorFirstName = $firstName;
		$this->authorLastName = $lastName;
		$this->price = $price;
		$this->discount = $discount;
	}

	public function fullName(){
		return $this->authorFirstName . ' ' . $this->authorLastName;
	}

	public function getPrice(){
		return $this->price - $this->discount;
	}

	public function getTitle(){
		return $this->title;
	}

	public function getDiscount(){
		return $this->discount;
	}

	public function setDiscount($discount){
		$this->discount = $discount;
	}

	public function getSummaryLine(){
		$result = '';
		$result .= $this->title;
		$result .= ' ' . $this->fullName();
		$result .= ' ' . $this->price;
		return $result;
	}

}