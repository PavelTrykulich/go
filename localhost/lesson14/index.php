<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'ShopProduct.Class.php';
require_once 'AudioProduct.Class.php';
require_once 'BookProduct.Class.php';

$audioProduct = new AudioProduct('Php is awesome','John', 'Doe', 30, 10, 200);
$audioProduct->setDiscount(20);
$bookProduct = new BookProduct('Super php','John', 'Doe', 30, 5, 450);
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h1><?=$audioProduct->getTitle()?></h1>
	<p><?=$audioProduct->getPrice();?></p>

	<div>
		<h2><?=$bookProduct->getTitle()?></h2>
		<p><?=$bookProduct->getPrice()?></p>
	</div>
</body>
</html>