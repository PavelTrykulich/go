<?php
class AudioProduct extends ShopProduct{
	protected $playTime = 0;

	public function __construct($title, $firstName, $lastName, $price, $discount, $playTime){
		parent::__construct($title, $firstName, $lastName, $price, $discount);
		$this->playTime = $playTime;
	}

	public function getSummaryLine(){
		$result = '';
		$result .= parent::getSummaryLine();
		$result .= ' ' . $this->playTime;
		return $result;
	}

	public function getPlayTime(){
		return $this->playTime;
	}

}