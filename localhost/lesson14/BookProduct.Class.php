<?php
class BookProduct extends ShopProduct{
	protected $pages = 0;

	public function __construct($title, $firstName, $lastName, $price, $discount, $pages){
		parent::__construct($title, $firstName, $lastName, $price, $discount);
		$this->pages = $pages;
	}

	public function getSummaryLine(){
		$result = '';
		$result .= parent::getSummaryLine();
		$result .= ' ' . $this->pages;
		return $result;
	}

	public function getPrice(){
		return $this->price;
	}

	public function getPages(){
		return $this->pages;
	}
}