<?php
session_start();

session_destroy();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Корзину очищено</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Heder block start-->
 <div>
    <? include "header.html";?>
 </div>
<!--Heder block end-->	
<center>
<h1>Корзину очищено</h1>
<h3><a href="index.php">Вернуться в магазин</a></h3>
</center>
<!--Footer block start-->
    <center>
    <div id="footer" class="alert alert-dark" role="alert">
      <span>Trykulich Pasha HomeWork 8 (c) 2018</span>
    </div>
  </center>
 <!--Footer block end-->
</body>
</html>