<?php

$products = [ ['title' => 'Adidas EQT',	   
			   'price_val' => 3500,  
			   'picture' => "images/EQT.jpg"
			  ],
			  ['title' =>'Adidas GAZELLE',
			   'price_val' => 3800,
			   'picture' => 'images/GAZELLE.jpg'
			  ],
			  ['title' =>'Adidas I-5923',
			   'price_val' => 2074,
			   'picture' => 'images/I-5923.jpg'
			  ],
			  ['title' =>'Adidas TUBULAR',
			   'price_val' => 2700,
			   'picture' => 'images/TUBULAR.jpg'
			  ],
			  ['title' =>'Adidas SUPERSTAR',
			   'price_val' => 2500,
			   'picture' => 'images/SUPERSTAR.jpg'
			  ],
			  ['title' =>'Adidas STAN SMITH',
			   'price_val' => 3075,
			   'picture' => 'images/STAN SMITH.jpg'
			  ],
			  ['title' =>'Adidas CLIMACOOL',
			   'price_val' => 1900,
			   'picture' => 'images/CLIMACOOL.jpg'
			  ],
			  ['title' =>'Adidas CRAZY',
			   'price_val' => 3900,
			   'picture' => 'images/CRAZY.jpg'
			  ],
			  ['title' =>'Adidas NMD',
			   'price_val' => 2130,
			   'picture' => 'images/NMD.jpg'
			  ],
			  ['title' =>'Adidas PHARRELL WILLIAMS',
			   'price_val' => 4150,
			   'picture' => 'images/PHARRELL WILLIAMS.jpg'
			  ]
            ];
?>