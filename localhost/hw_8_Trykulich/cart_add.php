<?php
session_start();

require "products.php";

$user_cart = [];
$key = $_GET['key'];

if(isset($_SESSION['user_cart'])){
    $user_cart = $_SESSION['user_cart'];
}

if(isset($user_cart[$key])){
  $user_cart[$key]++;
}else{
  $user_cart[$key] = 1;
}

$_SESSION['user_cart']=$user_cart;

?>

<!DOCTYPE html>
<html>  
<head>
  <meta charset="utf-8">
  <title>Товар добавлено</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Heder block start-->
 <div>
    <? include "header.html";?>
 </div>
<!--Heder block end-->

<!-- Content block start -->
<center>
<h1>Товар успешно добавлен в корзину</h1>
<h3><a href="index.php">Вернуться в магазин</a></h3>
</center>
<!--Content block end-->

<!--Footer block start-->
    <div>
    <? include "footer.html";?>
    </div>
<!--Footer block end-->
</body>
</html>