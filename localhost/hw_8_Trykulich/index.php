<?php
session_start();

require "products.php";

$i = 1;//counter for style 
 
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Адидас</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Heder block start-->
 <div>
    <? include "header.html";?>
 </div>
<!--Heder block end-->

<!-- Content block start -->


<div>
<p><h2 class="text"><center>ADIDAS</center></h2></p>

<table class="table">
      <thead class="table-primary">
       <tr>
          <th scope="col">№</th>
          <th scope="col">Фото</th>
          <th scope="col"><h5>Модель</h5></th>
          <th scope="col"><h5>Цена</h5></th>  
          <th scope="col"></th>
       </tr>
        </thead>
        <tbody class="font-weight-bold">     
                      
                     <?php 
                        foreach ($products as $key => $shoes):?> 
                         <tr <?php //use if for change the color of the rows
                         if($key % 2 != 0):?>
                              class="table-secondary"
                           <?php endif?>
                         
                         >                            
                            <td><?=$i++?></td>
                            <td>
                            <img src="<?=$shoes['picture'];?> " width="100" height="100">
                            </td>

                             <td><?=$shoes['title']?></td> 
                             <td><?=round($shoes['price_val'])
                             . ' uah'?></td>

                              <td><a href="cart_add.php?key=<?=$key?> ">Купить</a></td>
                           </tr> 
                          
                         <?php endforeach ?>
                                                          
         </tbody>
    </table>
</div>  

<div>

<p><h2 class="text"><center>Корзина</center></h2></p>

<!--cleaning the basket-->
<a href="delses.php"><center><h4>Очистить корзину</h4></center></a>

<?php if(isset($_SESSION['user_cart'])): ?><!--checking for Empty Basket-->

<table class="table">
      <thead class="table-primary">
       <tr>
        
          <th scope="col">Фото</th>
          <th scope="col"><h5>Модель</h5></th>
          <th scope="col"><h5>Цена</h5></th>
          <th scope="col"><h5>Количество</h5></th>
               
       </tr>
        </thead>  
        <tbody class="font-weight-bold">     
                                            
               <?php if(isset($_SESSION['user_cart'])):?>
                      <div>

                      <?php foreach ($_SESSION['user_cart'] as $productKey => $productValue): ?>
                                                     
                       <tr>                                           
                             <td>
                            <img src="<?=$products[$productKey]['picture'];?> " width="100" height="100">
                             </td>

                             <td><?=$products[$productKey]['title'];?></td> 
                             <td><?=$products[$productKey]['price_val']?></td> 
                             <td><?=$productValue?></td>                                                  
                       </tr>  
                       
                  <?php 
                    //amount of all goods
                    $all += ($products[$productKey]['price_val']*$productValue);endforeach;
                      endif;   
                      echo "<center><h3>Сумма всех товаров " . $all . " uah </h3></center>";?>                        
                        
                    </div>                                                           
         </tbody>
    </table>

   <?php else: ?> 

    <h1><center>Корзина пустая</center></h1>

   <?php endif; ?>

</div>  

<!--Content block end-->

<!--Footer block start-->
    <div>
    <? include "footer.html";?>
    </div>
<!--Footer block end-->
</body>
</html>