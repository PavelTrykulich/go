<?php
/**
 * Created by PhpStorm.
 * User: Pasha
 * Date: 13.05.2018
 * Time: 12:18
 */

class Food extends Item
{
    public function getPrice()
        {
            return $this->price - 10;
        }

        static public function getType()
        {
            return 'food';
        }

        public function getSummaryLine ()
        {
            $str = ' ';
            $str .= '<td>' . $this->title . '</td>';
            $str .= '<td>' . static::getType() . '</td>';
            $str .= '<td>' . $this->getPrice() . '</td>';
            return $str;
        }
}