<?php

class ItemsWriter
{
    protected $items = [];

    public function addItem(Writable $object)
    {
        return $this->items[] = $object;
    }

    public function write()
    {
        $str = ' ';

        foreach ($this->items as $item)
        {
            $str .= '<tr>' . $item->getSummaryLine() . '</tr>';
        }

        return $str;
    }
    public function getItems(){
        return $this->items;
    }
}