<?php

abstract class Item implements Writable
{
    protected $title = ' ';
    protected $type = ' ';
    protected $price = 0;

    public function __construct($title,$price)
    {
        $this->title = $title;
        $this->price = $price;
    }

    public function getTitle()
    {
        return $this->title;
    }

    static public function getType()
    {
        return self::$type = 'base_item';
    }

    public function getSummaryLine ()
    {
        $str = ' ';
        $str .= '<td>' . $this->title . '</td>';
        $str .= '<td>' . Item::getType() . '</td>';
        $str .= '<td>' . $this->price . '</td>';
        return $str;
    }

    abstract public function getPrice();
}