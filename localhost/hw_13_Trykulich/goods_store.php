<?php
require_once 'connection_db.php';

if(!isset($_POST['title'])) {
	header('Location:goods_create.php');
	die();
}

$title_text = htmlspecialchars($_POST['title'], ENT_QUOTES, 'utf-8');
$price_text = htmlspecialchars($_POST['price'], ENT_QUOTES, 'utf-8');
$type_text = htmlspecialchars($_POST['type'], ENT_QUOTES, 'utf-8');
$description_text = htmlspecialchars($_POST['description'], ENT_QUOTES, 'utf-8');


try{
	$sql = 'INSERT INTO goods SET
			title = "'.$title_text.'",
			price  = "'.$price_text.'",
			type = "'.$type_text.'",
			description ="'.$description_text.'"
	';
	$pdo->exec($sql);
	header('Location:index.php');

}catch(Exception $e){
	echo "Error adding test data" . $e->getMessage();
}