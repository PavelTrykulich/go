<?php
require_once 'connection_db.php';
$id = $_POST['id'];
try {
	$sql = 'DELETE from goods WHERE id=:id';
	$x = $pdo->prepare($sql);
	$x->bindValue(':id', $id);
	$x->execute();
	header('Location:index.php');
} catch (Exception $e) {
	echo 'Error'.$e->getMessage();
	die();
}
?>