<?php
if(!isset($_GET['id'])){
	header('Location:index.php');
}
require_once 'connection_db.php';

try{
	$id = (int)$_GET['id'];
	$sql = "SELECT * FROM goods WHERE id = :id";
	$x = $pdo->prepare($sql);
	$x->bindValue(':id', $id);
	$x->execute();
	$goods = $x->fetch();
}catch(Exception $e){
	echo 'Error'.$e->getMessage();
	die();
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title><?=$goods['title']?></title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Header block start-->
<div>
    <? include "header.html";?>
</div>
<!--Header block end-->


<!--Content blocck start-->
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Title</th>
      <th scope="col">Price</th>
      <th scope="col">Type</th>
      <th scope="col">Description</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
     
  <tbody>
    <tr>
    	<td><?=$goods['title']?></td>
    	<td><?=$goods['price']?></td>
    	<td><?=$goods['type']?></td>
    	<td><?=$goods['description']?></td>
    </tr>        
  </tbody>
</table>
<center><b><a href="index.php">Back</a></b></center>
<!--Content blocck end-->
    
<!--Footer block start-->
<div>
    <? include "footer.html";?>
</div>
<!--Footer block end-->

</body>
</html>