<?php
require_once 'connection_db.php';
include 'goods.array.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Заполнить таблицу данными из масива</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>


<!--Header block start-->
<div>
    <? include "header.html";?>
</div>
<!--Header block end-->

<?php
try{
	$sql = 'INSERT INTO goods SET

			title = :title,
			price = :price,
			type = :type,
			description = :description
	';
	
	$pdoStatement = $pdo->prepare($sql);

	foreach($article as $value){
		$pdoStatement->bindValue(':title', $value['title']);
		$pdoStatement->bindValue(':price', $value['price']);
		$pdoStatement->bindValue(':type', $value['type']);
		$pdoStatement->bindValue(':description', $value['description']);
		$pdoStatement->execute();
	}


	echo "Данные из масива добавлены";
	die();
	?>
	  
<?php
}catch(Exception $e){
	echo "Ошибка добавления" . $e->getMessage();

}?>

<!--Footer block start-->
<div>
    <? include "footer.html";?>
</div>
<!--Footer block end-->
</body>
</html>