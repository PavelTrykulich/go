<?php
require 'goods.array.php';

require_once 'connection_db.php';

try{
  $sql = 'SELECT * from goods';
  $result = $pdo->query($sql);

}catch(Exception $e){
  echo "Не удалось получить данные!";
  
}

$resultArray = $result->fetchAll();

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Goods</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Header block start-->
<div>
    <? include "header.html";?>
</div>
<!--Header block end-->


<!--Content blocck start-->
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Title</th>
      <th scope="col">Price</th>
      <th scope="col">Type</th>
      <th scope="col">Description</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
     
  <tbody>
    <?php 
    foreach($resultArray as $value):?>
    <tr>
    
      <td><a href="goods_show.php?id=<?=$value['id']?>"> <?=$value['title'];?></a></td>
      <td><?=$value['price'];?></td>
      <td><?=$value['type'];?></td>
      <td><?=$value['description'];?></td>
    
      <td><a href="goods_edit.php?id=<?=$value['id']?>">Edit</a></td>
      <td><a href="goods_delete.php?id=<?=$value['id']?>">Delete</a></td>
    
    </tr> 
     <?php endforeach;?>        
  </tbody>
</table>
<!--Content blocck end-->
    
<!--Footer block start-->
<div>
    <? include "footer.html";?>
</div>
<!--Footer block end-->

</body>
</html>