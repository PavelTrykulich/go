<?php
require_once 'connection_db.php';

$title = $_POST['title'];
$price = htmlspecialchars($_POST['price'], ENT_QUOTES, 'utf-8');
$type = htmlspecialchars($_POST['type'], ENT_QUOTES, 'utf-8');
$description = htmlspecialchars($_POST['description'], ENT_QUOTES, 'utf-8');
$id = htmlspecialchars($_POST['id'], ENT_QUOTES, 'utf-8');

try {
	$sql = 'UPDATE goods 
		SET title = :title,
		    price = :price,
			type = :type,
		 	description = :description
		WHERE id = :id
	';

	$x = $pdo->prepare($sql);
	$x->bindValue(':title', $title);
	$x->bindValue(':price', $price);
	$x->bindValue(':type', $type);
	$x->bindValue(':description', $description);
	$x->bindValue(':id', $id);
	$x->execute();
	header('Location:goods_show.php?id='.$id);
	die();

} catch (Exception $e) {
	echo 'Error'.$e->getMessage();
	die();
}


?>