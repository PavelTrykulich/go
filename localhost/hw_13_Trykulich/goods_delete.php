<?php
require_once 'connection_db.php';

try{
	$id = (int)$_GET['id'];
	$sql = "SELECT id, title FROM goods WHERE id = :id";
	$x = $pdo->prepare($sql);
	$x->bindValue(':id', $id);
	$x->execute();
	$goods = $x->fetch();
}catch(Exception $e){
	echo 'Error'.$e->getMessage();
	die();
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Delete</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Header block start-->
<div>
    <? include "header.html";?>
</div>
<!--Header block end-->


<!--Content blocck start-->
<center>

    <h1>Do you realy want to delete <?=$goods['title']?>?</h1>
	<form action="goods_destroy.php" method="post">

		<input type="hidden" name="id" value='<?=$goods['id']?>'>
		<button type='submit'>Yes</button>
		
	</form>

</center>
<!--Content blocck end-->
    
<!--Footer block start-->
<div>
    <? include "footer.html";?>
</div>
<!--Footer block end-->

</body>
</html>