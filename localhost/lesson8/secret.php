<?php
session_start();
if(!isset($_SESSION['logged_member'])){
	header('Location: index.php');
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Admin panel</title>
</head>
<body>
	<h1>Welcome to dashboard dear <?=$_SESSION['logged_member']?>!</h1>
	<a href="logout.php">Logout</a>
</body>
</html>