<?php

$products = [ ['title' => 'Adidas EQT',
			   'price_type' => 'uah',
			   'price_val' => 3500,
			   'discount_type' => 'percent',
			   'discount_val' => 15,
			   'picture' => "images/EQT.jpg"
			  ],
			  ['title' =>'Adidas GAZELLE',
			   'price_type' => 'usd',
			   'price_val' => 80,
			   'discount_type' => 'percent',
			   'discount_val' => 10,
			   'picture' => 'images/GAZELLE.jpg'
			  ],
			  ['title' =>'Adidas I-5923',
			   'price_type' => 'eur',
			   'price_val' => 74,
			   'discount_type' => 'percent',
			   'discount_val' => 15,
			   'picture' => 'images/I-5923.jpg'
			  ],
			  ['title' =>'Adidas TUBULAR',
			   'price_type' => 'uah',
			   'price_val' => 2700,
			   'discount_type' => 'value',
			   'discount_val' => 200,
			   'picture' => 'images/TUBULAR.jpg'
			  ],
			  ['title' =>'Adidas SUPERSTAR',
			   'price_type' => 'uah',
			   'price_val' => 2500,
			   'discount_type' => 'value',
			   'discount_val' => 300,
			   'picture' => 'images/SUPERSTAR.jpg'
			  ],
			  ['title' =>'Adidas STAN SMITH',
			   'price_type' => 'usd',
			   'price_val' => 75,
			   'discount_type' => 'value',
			   'discount_val' => 10,
			   'picture' => 'images/STAN SMITH.jpg'
			  ],
			  ['title' =>'Adidas CLIMACOOL',
			   'price_type' => 'usd',
			   'price_val' => 100,
			   'discount_type' => 'value',
			   'discount_val' => 25,
			   'picture' => 'images/CLIMACOOL.jpg'
			  ],
			  ['title' =>'Adidas CRAZY',
			   'price_type' => 'uah',
			   'price_val' => 3900,
			   'discount_type' => 'percent',
			   'discount_val' => 25,
			   'picture' => 'images/CRAZY.jpg'
			  ],
			  ['title' =>'Adidas NMD',
			   'price_type' => 'eur',
			   'price_val' => 130,
			   'discount_type' => 'percent',
			   'discount_val' => 25,
			   'picture' => 'images/NMD.jpg'
			  ],
			  ['title' =>'Adidas PHARRELL WILLIAMS',
			   'price_type' => 'eur',
			   'price_val' => 150,
			   'discount_type' => 'value',
			   'discount_val' => 20,
			   'picture' => 'images/PHARRELL WILLIAMS.jpg'
			  ]
            ];
?>