
<?php
include "currencysArrey.php";
include "products.php";
include "functions.php";

$i = 1;//counter for style 

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Адидас</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Heder block start-->
 <div>
    <? include "header.html";?>
 </div>
<!--Heder block end-->

<!-- Content block start -->
<div>
<p><h2 class="text"><center>ADIDAS</center></h2></p>
<table class="table">
      <thead class="table-primary">
       <tr>
          <th scope="col"></th>
          <th scope="col"><h5>Модель</h5></th>
          <th scope="col"><h5>Цена</h5></th>
          <th scope="col"><h5>Скидка</h5></th>
          <th scope="col"><h5>Цена со скидкой</h5></th>
        
       </tr>
        </thead>
        <tbody class="font-weight-bold">     
                      
                     <?php 
                        foreach ($products as $key => $shoes):?> 
                         <tr <?php //use if for change the color of the rows
                         if($key % 2 != 0):?>
                              class="table-secondary"
                           <?php endif?>
                         
                         >                            
                             <td>
                            <img src="<?=$shoes['picture'];?> " width="100" height="100">
                             </td>

                             <td><?=$shoes['title']?></td> 
                             <td><?=round(makeUah($shoes['price_type'],
                             $shoes['price_val'])) . ' uah'?></td>

                             <td><?=round(makeUah($shoes['price_type'],
                                $shoes['price_val']) -  Discount($shoes['discount_type'],
                                $shoes['discount_val'],$shoes['price_type'],
                                $shoes['price_val'])) . ' uah'?>
                             </td>

                             <td><?=round(Discount($shoes['discount_type'],
                                $shoes['discount_val'],$shoes['price_type'],
                                $shoes['price_val'])) . ' uah'?></td>
                           </tr> 
                          
                         <?php endforeach ?>                                                             
         </tbody>
    </table>
</div>  
<!--Content block end-->

<!--Footer block start-->
    <div>
    <? include "footer.html";?>
    </div>
<!--Footer block end-->
</body>
</html>