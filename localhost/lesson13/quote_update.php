<?php
require_once 'db_connection.php';

$quote_text = $_POST['quote_text'];
$quote_full_text = $_POST['quote_full_text'];
$quote_date = $_POST['quote_date'];
$id = $_POST['id'];

try {
	$sql = 'UPDATE quotes 
		SET quote_text = :quote_text,
		 quote_full_text = :quote_full_text,
		 quote_date = :quote_date
		 WHERE id = :id
	';

	$x = $pdo->prepare($sql);
	$x->bindValue(':quote_text', $quote_text);
	$x->bindValue(':quote_full_text', $quote_full_text);
	$x->bindValue(':quote_date', $quote_date);
	$x->bindValue(':id', $id);
	$x->execute();
	header('Location:quote_show.php?id='.$id);
	die();

} catch (Exception $e) {
	echo 'Error'.$e->getMessage();
	die();
}