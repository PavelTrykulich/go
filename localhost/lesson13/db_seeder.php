<?php
require_once 'db_connection.php';

$quotes = [
	[
		'quote_text' => 'Php is awesome',
		'quote_date' => '2018-04-02',
		'quote_full_text' => 'Php is awesome'
	],
	[
		'quote_text' => 'HTML is awesome',
		'quote_date' => '2018-04-03',
		'quote_full_text' => 'HTML is awesome'
	],
	[
		'quote_text' => 'CSS is awesome',
		'quote_date' => '2018-04-04',
		'quote_full_text' => 'CSS is awesome'
	],
];

try{
	$sql = 'INSERT INTO quotes SET
			quote_text = :quote_text,
			quote_date = :quote_date,
			quote_full_text = :quote_full_text
	';
	$pdoStatement = $pdo->prepare($sql);

	foreach($goods as $value){
		$pdoStatement->bindValue(':title', $goods['title']);
		$pdoStatement->bindValue(':price', $goods['price']);
		$pdoStatement->bindValue(':type', $goods['type']);
		$pdoStatement->bindValue(':description', $goods['description']);
		$pdoStatement->execute();
	}


	echo "Test data added";
	die();

}catch(Exception $e){
	echo "Error adding test data" . $e->getMessage();
}