<?php
if(!isset($_GET['id'])){
	header('Location:index.php');
}
require_once 'db_connection.php';

try{
	$id = (int)$_GET['id'];
	$sql = "SELECT * FROM quotes WHERE id = :id";
	$x = $pdo->prepare($sql);
	$x->bindValue(':id', $id);
	$x->execute();
	$quote = $x->fetch();
}catch(Exception $e){
	echo 'Error'.$e->getMessage();
	die();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title><?=$quote['quote_text']?></title>
</head>
<body>
	<h1><?=$quote['quote_text']?></h1>
	<p><?=$quote['quote_full_text']?></p>
	<br>
	<a href="index.php">Back</a>
</body>
</html>