<?php
if(!isset($_GET['id'])){
	header('Location:index.php');
}
require_once 'db_connection.php';

try{
	$id = (int)$_GET['id'];
	$sql = "SELECT * FROM quotes WHERE id = :id";
	$x = $pdo->prepare($sql);
	$x->bindValue(':id', $id);
	$x->execute();
	$quote = $x->fetch();
}catch(Exception $e){
	echo 'Error'.$e->getMessage();
	die();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit</title>
</head>
<body>
	<form action='quote_update.php' method="post">
		<label for='quote_text'>Enter quote text:</label>
		<br>
		<textarea name='quote_text' id="quote_text"><?=$quote['quote_text']?></textarea>
		<br>
		<label for="quote_date">Enter date:</label>
		<br>
		<input type="date" name="quote_date" id="quote_date" value="<?=$quote['quote_date']?>">
		<br>
		<label for='quote_full_text'>Enter Full Text:</label>
		<br>
		<textarea name="quote_full_text" id='quote_full_text'><?=$quote['quote_full_text']?></textarea>
		<input type="hidden" name="id" value='<?=$quote['id']?>'>
		<button type='submit'>Save</button>
	</form>
</body>
</html>
