<?php
if(!isset($_GET['id'])){
	header('Location:index.php');
}
require_once 'connection_db.php';

try{
	$id = (int)$_GET['id'];
	$sql = "SELECT id, quote_text FROM quotes WHERE id = :id";
	$x = $pdo->prepare($sql);
	$x->bindValue(':id', $id);
	$x->execute();
	$quote = $x->fetch();
}catch(Exception $e){
	echo 'Error'.$e->getMessage();
	die();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Delete</title>
</head>
<body>
	<h1>Do you realy want to delete <?=$goods['title']?>?</h1>
	<form action="goods_destroy.php" method="post">
		<input type="hidden" name="id" value='<?=$goods['id']?>'>
		<button type='submit'>Yes</button>
	</form>
</body>
</html>