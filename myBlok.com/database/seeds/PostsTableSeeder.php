<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
             'title' => 'Php',
             'slug' => 'Php',
             'short_description' => 'Php is awesome',
             'body' => 'text text text'
            ],
            [
             'title' => 'Js',
             'slug' => 'Js',
             'short_description' => 'Js is awesome',
             'body' => 'text text text'
            ],
            [
             'title' => 'Css',
             'slug' => 'Css',
             'short_description' => 'Css is awesome',
             'body' => 'text text text'
            ]
            ]);
    }
}
