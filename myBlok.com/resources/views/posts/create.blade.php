@extends('layouts.main')

@section('jumbotron')
    <h1 class="display-3">Create post</h1>
    <p>Here we can create new post</p>
@endsection

@section('main_content')




    <form action="/posts" method="post">

     @include('layouts.embed.errors')

        {{csrf_field()}}
        <div class="form-group">
            <label for="title">Title</label>
            <input class="form-control" type="text" name="title" id="title">
        </div>

        <div class="form-group">
            <label for="slug">Slug</label>
            <input class="form-control" type="text" name="slug" id="slug">
        </div>
        <div class="form-group">
            <label for="short_description">Short description</label>
            <textarea class="form-control" type="text" name="short_description" id="short_description"></textarea>
        </div>
        <div class="form-group">
            <label for="body">Body</label>
            <textarea class="form-control" type="text" name="body" id="body"></textarea>
        </div>
        <div class="form-group">
            <button class="btn badge-primary" type="submit">Create</button>
        </div>


    </form>
@endsection