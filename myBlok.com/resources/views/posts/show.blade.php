@extends('layouts.main')

@section('jumbotron')
    <h1 class="display-3">{{$post->title}}</h1>
    <p>{{$post->short_description}}</p>
@endsection

@section('main_content')
    <div class="col-md-12">
        <p>{{$post->body}}</p>
    </div>
@endsection