<?php

namespace App\Http\Controllers;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PostController extends Controller
{
    public function index(){
        $posts = Post::all();
        return view('posts.index')->with(compact('posts'));
     }

     public function show(Post $post){
        return view('posts.show')->with(compact('post'));
     }
     public function create(){
        return view('posts.create');
     }

     public function store(){
        $post = new Post();
        $post->title = request()->post('title');
        $post->slug = request()->post('slug');
        $post->short_description = request()->post('short_description');
        $post->body = request()->post('body');

        $post->save();

        return redirect('/posts');
     }

}
